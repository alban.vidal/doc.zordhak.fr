% Générer un clé ed25519 personnalisée

**ATTENTION** : La clé générée ci-dessous n'a pas de mot de passe !

La date est au format **AAAA-MM-JJ**

--------------------

### Génération du couple de clé publique/privée
```bash
ssh-keygen -t ed25519 -N "" -C "${USER}_$(hostname)_$(date +%F)" -f ~/.ssh/id_ed25519_${USER}_$(hostname)_$(date +%F)
```
--------------------

### Détail sur les options de génération de la clé

+ -t : Type de clé, ici `ed25519`
+ -N : Mot de passe de la clé, ici **`SANS MOT DE PASSE`**
+ -C : Commentaire de la clé, ici `<UTILISATEUR>_<HOSTNAME>_<DATE>`
+ -f : Fichier de sortie, ici `id_ed25519_<UTILISATEUR>_<HOSTNAME>_<DATE>`

--------------------

### Format de sortie de la clé privée
```
id_ed25519_<UTILISATEUR>_<HOSTNAME>_<DATE>
# Exemple
id_ed25519_root_dnsmast_2018-05-06
```

### Contenu de la clé pubique
```
ssh-ed25519 <EMPREINTE> <UTILISATEUR>_<HOSTNAME>_<DATE>
# Exemple
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINSibsG1XbwbX0W/YTQiI/NIZm3gWJDZDG8CCqcVjiWV root_dnsmast_2018-05-06
```

