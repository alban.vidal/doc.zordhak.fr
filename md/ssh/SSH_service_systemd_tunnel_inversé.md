% ssh - service systemd tunnel inversé

Le but de cette doc est de décrire comment mettre en place un service systemd permettant l'établissement d'un tunnel ssh inversé.

--------------------

## Contexte

* « Client ssh » : Serveur ou PC qui initie le tunnel ssh inversé
* « Serveur ssh inversé » : Serveur servant de rebond ssh
* « PC d'admin » : PC pouvant se connecter au « client ssh » via le « serveur ssh inversé »

--------------------

## Prérequis

* Avoir une clé privée ssh sur « Client ssh »
* Avoir une clé privée ssh sur « PC d'admin »
* Avoir la clé publique de « Client ssh » sur « Serveur ssh inversé »
* Avoir la clé publique de « PC d'admin » sur « Serveur ssh inversé » et sur « Client ssh »

--------------------

## Mise en place service de tunnel ssh systemd sur « Client ssh »

Création du timer, fichier `/etc/systemd/system/ssh-tunnel-inverse.service`

```
[Unit]
Description=Tunnel inversé vers vp1
Wants=network-online.target
After=network-online.target network.target
Conflicts=shutdown.target
Before=shutdown.target

[Service]
ExecStart=ssh -g -N -T -o "ServerAliveInterval 10" -o StrictHostKeyChecking=no -o "ExitOnForwardFailure yes" -R 1234:localhost:22 -p 5678 utilisateur-inverse@serveur-ssh-inverse
KillMode=process
Restart=always
RestartSec=5s
Type=simple

[Install]
WantedBy=multi-user.target
```

Éléments à modifier :

* `1234` est le port d'écoute local sur le « Serveur ssh inversé »
* `serveur-ssh-inverse` d'adresse IP ou le nom du « Serveur ssh inversé »
* `utilisateur-inverse` est l'utilisateur sur « Serveur ssh inversé » qui a la clé publique de « Client ssh »
* `5678` est le port d'écoute ssh sur « Serveur ssh inversé »
* `22` est le port d'écoute ssh sur « Client ssh »

## Activation du service et vérification

Activation du service sur « Client ssh » et vérification
```
systemctl enable --now ssh-tunnel-inverse.service
systemctl is-active ssh-tunnel-inverse.service
```

Vérifications sur « Client ssh » et « Serveur ssh inversé »
```
ss -lptn # pour vérifier que les ports configurés sont bien à l'écoute
```

## Connexion sur « Client ssh » via « Serveur ssh inversé » depuis « PC d'admin »

```
ssh -J utilisateur-tunnel@serveur-ssh-inverse:5678 -p 1234 utilisateur-client-ssh@localhost
```

Explications :

* `utilisateur-tunnel` est l'utilisateur utilisé sur « Serveur ssh inversé »
* `serveur-ssh-inverse` d'adresse IP ou le nom du « Serveur ssh inversé »
* `5678` est le port d'écoute ssh sur « Serveur ssh inversé »
* `1234` est le port d'écoute local sur le « Serveur ssh inversé »
* `utilisateur-client-ssh` est l'utilisateur à se connecter sur « Client ssh »
