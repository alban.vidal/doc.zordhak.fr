% GPG - Envoyer un mail chiffré en CLI

----------------------------------------

Dans les deux cas nous devons déjà avoir la clé publique localement.

Exemple pour télécharger une clé GPG publique :

```
gpg --receive-keys GPGID
```

**GPGID** est à remplacer par l'identifiant de la clé GPG.

Si non fonctionnel, il est probable qu'il soit nécessaire de spécifier le serveur des clés, possible comme suit :

gpg --receive-keys --keyserver hkp://keys.gnupg.net GPGID

### Méthode 1

Avec cette méthode, nous devons avoir la clé comme confiance « ultime ».

```
echo "Coucou je suis un test avec clé Ultime" \
    | gpg --encrypt --armor --no-tty -r user@example.com \
    | mail -s "Sujet du mail" user@example.com
```

### Méthode 2

Avec cette méthode, pas besoin d'avoir une confiance « ultime », car nous n'allons faire aucune vérification sur la clé publique.

```
echo "Coucou je suis un test SANS clé Ultime" \
    | gpg --encrypt --armor --yes --no-tty --trust-model always -r user@example.com \
    | mail -s "Sujet du mail" user@example.com
```

----------------------------------------

### Détail des options

**TODO**

