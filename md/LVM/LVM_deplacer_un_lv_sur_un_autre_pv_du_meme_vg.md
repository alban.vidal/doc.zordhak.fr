% LVM : Déplacer un LV sur un autre PV (mais faisant partit du même VG)

 + On déplace le LV **lv_srv_sauvegarde** qui est dans le **vg_srv**
 + à la fois sur les PV **/dev/md2** et **/dev/md3**, et on veut libérer **/dev/md2** :

    pvmove -n lv_srv_sauvegarde /dev/md2 /dev/md3

