% LVM et les segments physiques

#### Afficher les LV présents sur les disques

    pvdisplay --maps

----------------------------------------------------------------------------------------------------

#### Afficher les LV présents sur un disque spécifique

    pvdisplay --maps /dev/md2

----------------------------------------------------------------------------------------------------

#### Affiche les périphériques d'un volume logique

    lvs --segments /dev/vg_srv/lv_srv_toto -o +lv_size,devices

*--segments pour un lvm permet d'avoir la taille en Go utilisé sur le disque physique pour le lv donné*   
*-o +lv_size,devices pour avoir la taille du LV ainsi que les périphériques block affiliés*

----------------------------------------------------------------------------------------------------

#### Affiche les périphériques d'un volume logique (détails)

    lvdisplay -m /dev/vg_srv/lv_srv_toto

----------------------------------------------------------------------------------------------------

#### Si c'est du mirroir (raid1), il faut ajouter l'option -a (all)

    lvdisplay -am /dev/vg_srv/lv_srv_toto

----------------------------------------------------------------------------------------------------

#### Synthèse d'un PV avec les LV présents, ainsi que la taille des LV

    pvs /dev/md2  --segments  -o +lv_name,lv_size


----------------------------------------------------------------------------------------------------

#### Synthèse complète pour tous les disques

    pvs --segments -o pv_name,pv_size,seg_size,vg_name,lv_name,lv_size,seg_pe_ranges

