% LVM et l'utilisation des snapshots

Nous allons travailler sur le VM **lv_data** présent sur le **vg_toto**

##### Créer un snap du LV

    lvcreate -L 1GB -s -n snap_data /dev/vg_toto/lv_data

ou

    lvcreate --size 1G --snapshot --name snap_data /dev/vg_toto/lv_data

--------------------------------------------------------------

##### Supprimer le snap - appliquer les changements
    lvremove /dev/vg_toto/snap_data

--------------------------------------------------------------

##### Merge le snap (rollback) - ne pas appliquer les changements

 1. démonter le volume :
    ```
    umount /dev/vg_toto/lv_data
    ```

 2. merge :
   ```
   lvconvert --merge /dev/vg_toto/snap_data
   ```

 3. et on remonte :
   ```
   mount /dev/vg_toto/snap_data /srv/data
   ```

