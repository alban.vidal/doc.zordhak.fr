% LVM et l'utilisation du Thin

## Préparation du système

#### Paquet à installer :

    apt install thin-provisioning-tools

--------------------------------------------------------------------------------

## Mise en place

#### Création du VG

    vgcreate vg_thin /dev/sdX

#### Création du pool lvm thin dans le VG (on lui donne la taille max que pourront avoir tous les LV thin contenu à l'interieur):

    lvcreate -L 100G -T vg_thin/thinPool

#### Création du volume thin à l'intérieur du pool thin :

**-V 2G => taille du volume**

    lvcreate -V2G -T vg_thin/thinPool -n lvThin_srv_lxc_test-thin


#### Créer un nouveau LV thin dans le pool :

    lvcreate -V2G -T vg_thin/thinPool -n lvThin_srv_lxc_test-ajout-LV-Thin

--------------------------------------------------------------------------------

##### Avoir les détails d'utilisation d'un pool Thin :

    lvs
    # ou pour avoir juste le pool Thin
    lvs vg_thin/thinPool

**Avec la même commande on peut aussi voir l'utilisation d'un LV Thin, et à quel pool il est attaché**

##### Augmenter la taille du pool thin :

    lvextend -L+10G vg_thin/thinPool

--------------------------------------------------------------------------------

##### Réparer les métadonnées d'un pool Thin

    lvconvert --repair vg_thin/thinPool

Utilise thin_repair - repair thin provisioning binary metadata from device/file to device/file

Plus d'info sur le man spécifique

    man thin_repair

--------------------------------------------------------------------------------

#### MANUEL

    man lvmthin

#### DOC EN LIGNE REDHAT :
[https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Logical_Volume_Manager_Administration/thinly_provisioned_volume_creation.html](Logical_Volume_Manager_Administration/thinly_provisioned_volume_creation)

#### Informations sur les SNAP VLM sur un LV Thin :
[https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Logical_Volume_Manager_Administration/thinly-provisioned_snapshot_volumes.html](Logical_Volume_Manager_Administration/thinly-provisioned_snapshot_volumes)

