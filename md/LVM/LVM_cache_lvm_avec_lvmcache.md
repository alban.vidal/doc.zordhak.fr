% Exemple d'utilisation de lvmcahe (SSD + HDD)

# Création d'un cache

Dans cet exemple nous avons un SSD (/dev/vdc) et un HDD (/dev/vdd)

Les données seront sur le HDD et le cache sur le SSD

1. Création des PV et du LV
   ```
   pvcreate /dev/vdc /dev/vdd
   vgcreate vg_data /dev/vdc /dev/vdd
   ```

2. Création du volume contenant les données
   ```
   lvcreate -L30G -n lv_data vg_data /dev/vdd
   ```

3. Création du cache sur le SSD (dont les métadonnées)
   ```
   lvcreate -n CacheDataLV -L 5G vg_data /dev/vdc
   lvcreate -n CacheMetaLV -L 12M vg_data /dev/vdc
   lvs -a vg_data
   ```

4. Création du pool cache
   ```
   lvconvert --type cache-pool --poolmetadata vg_data/CacheMetaLV vg_data/CacheDataLV
   lvs -a vg_data
   ```

5. Application du pool de cache sur le volume recevant les données
   ```
   lvconvert --type cache --cachepool vg_data/CacheDataLV vg_data/lv_data
   lvs -a vg_data
   ```

6. Maintenant que le cache est créée, on empèche la création de nouveaux volumes sur le SSD
   ```
   pvchange --allocatable n /dev/vdc
   ```

   Voir l'état (si on peut ajouter un LV ou non sur un PV)
   ```
   pvdisplay /dev/vdc|grep Allocatable
   # (No si on ne peux pas en créer de nouveaux)
   # ou 
   pvs /dev/vdc
   ```
    Dans les Attr :
    + **a** => allouable a la création d'un nouveau lv - (a)llocatable
    + **n** => n'est pas allouable - (u)sed (but not allocatable)

   si on désire le réactiver plus tard
   ```
   pvchange --allocatable y /dev/vdc
   ```

Il est à noter qu'il est possible de faire la cléation du cache, des métadonnées et l'application sur le LV de données avec une seule ligne :
```
lvcreate --type cache-pool -L CacheSize -n CachePoolLV VG FastPVs
```

# Suppression d'un cache

Plusieurs méthodes suivant si on veut garder les volumes de caches ou non

+ Détacher le cache des données (split)
  ```
  lvconvert --splitcache vg_data/CacheDataLV
  ```

+ Supprimer le pool de cache
  ```
  lvremove vg_data/CacheDataLV
  # ou
  lvconvert --uncache VG/CacheLV
  ```

#### NOTES

Dans le manuel est aussi présenté un cache en mirroir (raid1) pour éviter les pannes de cache

### Modes de cache :

+ **writethrough** (défaut) : écrit les données en direct sur le disque lent (ici HDD)
+ **writeback** : écrit les données sur le cache, et plus tard sur le HDD, plus performant mais peut provoquer de les perte de données si le cache n'est plus disponible

Pour spécifier le mode ou le modifier, utiliser l'option `--cachemode`

Pour afficher le mode actuel :
```
lvs -o+cache_mode vg_data/lv_data
```

#### Manuel

```
man lvmcache
```
