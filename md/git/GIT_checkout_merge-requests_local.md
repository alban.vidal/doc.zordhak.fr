% Git - Checkout local d'une Merge-Requests

--------------------

[Lien vers la dog Gitlab](https://docs.gitlab.com/ee/user/project/merge_requests/reviewing_and_managing_merge_requests.html#checkout-merge-requests-locally)

--------------------

## Méthode 1 - Configuration globale

### Alias à mettre dans la conf locale

Fichier de conf `~/.gitconfig`

```
[alias]
    mr = !sh -c 'git fetch $1 merge-requests/$2/head:mr-$1-$2 && git checkout mr-$1-$2' -
```

### Utilisation

Exemple, remote **origin** et merge-requests **123**

```
git mr origin 123
```

Cela la créer la branche locale **mr-origin-123**

--------------------

## Méthode 2 - Configuration d'un dépôt spécifique (avec fetch possible)

Exemple actuel de la conf du dépôt local (dans le fichier `.git/config`) :
```
[remote "origin"]
  url = https://gitlab.com/gitlab-org/gitlab-foss.git
  fetch = +refs/heads/*:refs/remotes/origin/*
```

Modifier le fichier de conf ou l'éditer avec la commande :
```
git config -e
```

Ajouter la configuration suivante :
```
fetch = +refs/merge-requests/*/head:refs/remotes/origin/merge-requests/*
```

Ce qui donne la configuration complète suivante :
```
[remote "origin"]
  url = https://gitlab.com/gitlab-org/gitlab-foss.git
  fetch = +refs/heads/*:refs/remotes/origin/*
  fetch = +refs/merge-requests/*/head:refs/remotes/origin/merge-requests/*
```

Maintenant avec un `fetch` nous avons la possibilité de voir les merge-requests :
```
git fetch origin

...
From https://gitlab.com/gitlab-org/gitlab-foss.git
 * [new ref]         refs/merge-requests/1/head -> origin/merge-requests/1
 * [new ref]         refs/merge-requests/2/head -> origin/merge-requests/2
...
```

Et pour faire un checkout :
```
git checkout origin/merge-requests/1
```
