% Désactivation CI Gitlab lors d'un push

----------------------------------------

## Méthode 1 - Ajouter une mention dans le message de commit

+ Inclure `[ci skip]` dans le message de commit
+ Inclure `[skip ci]` dans le message de commit

## Méthode 2 - Ajouter une option au push

+ Ajouter l'option `ci.skip` dans la commande push.

Ce qui donne :

```
git push -o ci.skip
```

----------------------------------------

Source : https://docs.gitlab.com/ee/ci/yaml/#skip-pipeline
