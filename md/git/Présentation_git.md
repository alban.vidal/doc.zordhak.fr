% Git - GitLab
% Alban Vidal
% 19 Avril 2019

![](https://dl.zordhak.fr/logo-git.png)

# 

## Logiciel de gestion de versions

+ VCS en anglais (version control system)
+ Permettant de stocker un ensemble de fichiers
+ Conserve la chronologie de toutes les modifications

# 

## VCS - Exemples

Quelques exemples projets utilisant des VCS

+ CVS - OpenBSD
+ SVN (Subversion) - Apache, Redmine
+ Mercurial - Mozilla, Python, OpenOffice.org
+ Bazaar - Ubuntu, MySQL, Inkscape
+ Git - [Noyau Linux](https://git.kernel.org/), [Debian](https://salsa.debian.org/), [VLC](https://github.com/videolan/vlc), [Android](https://android.googlesource.com/), [Gnome](https://gitlab.gnome.org/GNOME), [Qt](https://github.com/qt)

#

## CVS

+ CVS (Concurrent Versions System)
+ À été largement utilisé par les projets de logiciels libres
+ Création - 1990
+ Dernière version - Mai 2008

#

## Subversion (SVN)

+ À été conçu pour remplacer CVS
+ Mêmes concepts que CVS
+ Création - 2000
+ Dernière version - Janvier 2019

#

## Mercurial, Bazaar et Git

+ Se valent globalement
+ Récents et puissants
+ Chacun a des avantages et des défauts

#

## Bazaar (bzr)

+ Création - Mars 2005
+ Dernière version - Février 2016

#

## Git

+ Créé par Linus Torvalds (auteur du noyau Linux)
+ Création - Avril 2005
+ Dernière version - Février 2019

#

## Forges Web

Système de gestion de développement colaboratif

Forges web d'hébergement Git :

+ [GitHub](https://github.com/)
+ [GitLab](https://about.gitlab.com/)
+ [FramaGit](https://framagit.org)

#

## GitHub

+ plus de 14 millions d'utilisateurs
+ plus de 35 millions de dépôts de projets
+ plus grand hébergeur de code source au monde
+ racheté en 2018 par Microsoft pour 7,5 milliards de dollars

#

## GitLab

À l'origine, GitLab était un logiciel libre.

Ensuite scindé en deux versions :

+ GitLab CE (Community Edition)
+ GitLab EE (Enterprise Edition)

GitLab CE peut donc être installé pour faire de l'auto-hergement

(notes)

#

## FramaGit

+ Forge logicielle de Framasoft
+ Reposant sur le logiciel Gitlab
+ Ouverte à tous
+ Limite de 42 projets par personne
+ Les projets peuvent être publics ou privés

#

## Démo time !

#

## LAB de démo

+ labadm (poste de travail / admin)
+ labsrv1 (serveur 1)
+ labsrv2 (serveur 2)

Un nginx est devant **srv1** et **srv2** en loadbalencer

#

## Installation des paquets

```bash
# Admin - Git
apt update && apt install git

# Serveurs - Git et Apache2
apt update && apt install git apache2
```

#

## Présentation GitLab

+ Interface Web
+ Groupes et sous-groupes
+ Projets

#

## Clé SSH d'admin

Création d'une clé au format **ed25519** contenant :

+ Le nom d'utilisateur
+ Le nom d'hôte
+ La date

```bash
ssh-keygen -t ed25519 -N "" -C "${USER}_$(hostname)_$(date +%F)" -f ~/.ssh/id_ed25519_${USER}_$(hostname)_$(date +%F)
# Lien symbolique pour ssh
ln -s ~/.ssh/id_ed25519_${USER}_$(hostname)_$(date +%F) ~/.ssh/id_ed25519

# Récupération de la clé publique
cat ~/.ssh/id_ed25519_${USER}_$(hostname)_$(date +%F).pub
```

#

## Ajout clé SSH dans GitLab

Pour faciliter l'authentification, nous ajoutons une clé SSH dans le profile de l'utilisateur 

`Settings => SSH Keys`

Y ajouter la clé précédemment générée dans le conteneur **labadm**

#

## Création de deux projets

Plateforme utilisé - [Framagit](https://framagit.org/)

+ demo-conf (vHosts apache2)
+ demo-www (sites web)

#

## Configuration Git - Admin

Définition nom et adresse de courriel

```bash
git config --global user.name "Alban Vidal"
git config --global user.email "alban.vidal@zordhak.fr"

# Vérification
git config --global --list
# ou
cat ~/.gitconfig
```

#

## Configuration Git - srv*

Définition nom et adresse de courriel

```bash
git config --global user.name "${HOSTNAME}"
git config --global user.email "${USER}-${HOSTNAME}@demo"

# Vérification
git config --global --list
```

#

## Clône des des dépôts - Admin

```
# Préparation répertoire git
mkdir /srv/git

# En SSH
git clone git@framagit.org:alban.vidal/demo-conf.git /srv/git/demo-conf
git clone git@framagit.org:alban.vidal/demo-www.git /srv/git/demo-www

ls -l /srv/git
```

#

## Clône des des dépôts

Sur **srv1** et **srv2**

Sans clé de déploiement nous ne pourrons pas clôner en SSH

```bash
# Préparation répertoire git
mkdir /srv/git

# En HTTPS
git clone https://framagit.org/alban.vidal/demo-conf.git /srv/git/demo-conf
git clone https://framagit.org/alban.vidal/demo-www.git /srv/git/demo-www

ls -l /srv/git
```

#

## Création des vHosts

Depuis **labadm**

```bash
cd /srv/git/demo-conf
mkdir sites-available
echo -e "<VirtualHost *:80>\n\tServerName demo-1.zordhak.fr\n\tDocumentRoot /var/www/demo-1\n</VirtualHost>" > sites-available/demo-1.conf
echo -e "<VirtualHost *:80>\n\tServerName demo-2.zordhak.fr\n\tDocumentRoot /var/www/demo-2\n</VirtualHost>" > sites-available/demo-2.conf

ls -l sites-available/

# Status du dépôt local
git status
```

#

## Envoie dans serveur Git (Conf)

Depuis **labadm**

```bash
# Ajout des deux nouveaux fichiers dans l'index
git add sites-available

# Enregistrer les modifications dans le dépôt local
git commit -m "XXXXX - Ajout des vHosts demo-1 et demo-2"
git status

# Envoie les modifications sur le serveur
git push origin master

git status
```

#

## Ajout des fichiers Web

Depuis **labadm**

```bash
cd /srv/git/demo-www
mkdir demo-1 demo-2

echo "Site 1" > demo-1/index.html
echo "Site 2" > demo-2/index.html

git status
```

#

## Envoie dans serveur Git (Web)

Depuis **labadm**

```bash
# Ajout des deux nouveaux fichiers dans l'index
git add demo-1 demo-2

# Enregistrer les modifications dans le dépôt local
git commit -m "XXXXX - Ajout des sites demo-1 et demo-2"

# Envoie les modifications sur le serveur
git push origin master

git status
```

#

## Récupération des données

Sur **srv1** et **srv2**

```bash
cd /srv/git/demo-conf
ls -l

# Récupération des mises à jour
git pull

# Status du dépôt local
git status

# Idem www
cd /srv/git/demo-www
git pull
ls -l
```

#

## Créations de liens

Sur **srv1** et **srv2**

```bash
rm -rf /etc/apache2/sites-available

# Création liens symboliques vers Git
# Apache2
ln -s /srv/git/demo-conf/sites-available/ /etc/apache2

# Sites
ln -s /srv/git/demo-www/demo-1 /var/www/
ln -s /srv/git/demo-www/demo-2 /var/www/

# Vérifications
ls -l /etc/apache2
ls -l /var/www
```

#

## Activation sites dans apache

Sur **srv1** et **srv2**

```bash
a2dissite 000-default
a2ensite demo-1
a2ensite demo-2

# Rechargement apache2
systemctl reload apache2

# Démo Web
# https://demo-1.zordhak.fr/
# https://demo-2.zordhak.fr/
```

#

## Modification d'un fichier

Retour sur le ct **labadm**

```bash
cd /srv/git/demo-www

# Ajout d'une ligne
echo "bla bla" >> demo-1/index.html

git status
git add demo-1/index.html
git commit -m "XXXXXX - ajout ligne bla bla"
git push origin master
```

#

## Récupération de la modification

Sur **srv1** et **srv2**

```bash
cd /srv/git/demo-www

cat demo-1/index.html
git pull
cat demo-1/index.html

# Voir les modifications
git diff HEAD~1
git diff HEAD~2
```

#

## Annulation d'une modification locale

Retour sur le ct **labadm**

```bash
cd /srv/git/demo-www

echo "erreur" >> demo-1/index.html
git status
# Voir la différence
git diff
# annulation
git checkout demo-1/index.html
git status
cat demo-1/index.html
```

#

## Quelques commandes supplémentaires

```bash
# Logs des commits
git log

# Logs des deux derniers commits
git log -1

# Afficher le ou les derniers commits
git show
git show -2

# Afficher un commit via son numéro
git show [n° de commit]
```

#

## Retour sur l'interface GitLab

+ Voir les documents
+ Voir les dommits

#

## Notes

Penser à TOUJOURS faire un `git pull` avant de faire un commit

[Quelques notes perso](https://doc.zordhak.fr/)

+ GIT annulation commit
+ GIT diff patch et email
+ GIT options de clone
+ GIT synchro d un fork

#

## Format Markdown

# Liens

+ [OpenClassRoom - Git](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)
+ [Git Site Web](https://git-scm.com/)
+ [Git Doc](https://git-scm.com/doc)
+ [GitLab CE - Doc utilisateurs](https://docs.gitlab.com/ce/user/index.html)
+ [GitLab CE - Nouveau dans git ou gitlab ?](https://docs.gitlab.com/ce/README.html#new-to-git-and-gitlab)
+ [Framasoft](https://framasoft.org/fr/)
+ [Framagit](https://framagit.org/)
+ [Framagit - Doc](https://docs.framasoft.org/fr/gitlab/)

# Liens - suite

+ [Wikipedia - Logiciel de gestion de versions](https://fr.wikipedia.org/wiki/Logiciel_de_gestion_de_versions)
+ [Wikipedia - Concurrent versions system](https://fr.wikipedia.org/wiki/Concurrent_versions_system)
+ [Wikipedia - Apache Subversion](https://fr.wikipedia.org/wiki/Apache_Subversion)
+ [Wikipedia - Bazaar](https://fr.wikipedia.org/wiki/Bazaar_(logiciel))
+ [Wikipedia - Git](https://fr.wikipedia.org/wiki/Git)
+ [Wikipedia - GitHub](https://fr.wikipedia.org/wiki/GitHub)
+ [Wikipedia - GitLab](https://fr.wikipedia.org/wiki/GitLab)

# FIN

--------------------

# Présentation au format reveal

## Paquets requis

```bash
apt install git pandoc texlive-latex-recommended texlive-xetex
```

# Présentation au format reveal (2)

## Téléchargement de reveal.js et présentation au format MD

```bash
git clone https://github.com/hakimel/reveal.js.git
wget https://doc.zordhak.fr/git -O Présentation_git.md
```

# Compilation

# Compiler au format HTML

```bash
pandoc -t revealjs -s Présentation_git.md -o Présentation_git.html
```

# Compiler au format HTML (tout en un)

```bash
pandoc -t revealjs -s Présentation_git.md -o Présentation_git.html --self-contained
```

# Compiler au format PDF

```bash
pandoc Présentation_git.md -o Présentation_git.pdf -V documentclass=report --table-of-contents
```

