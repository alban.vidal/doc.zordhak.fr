% Sauvegarde fichiers sensibles

# Création du git local

```
mkdir /srv/.local-git
cd /srv/.local-git
git init
git config user.name "$HOSTNAME"
git config user.email "systemd-path@$HOSTNAME"
```

--------------

# Commit initial

```
mkdir /srv/.local-git/etc
cp /etc/fstab /etc/hosts /srv/.local-git/etc
cd /srv/.local-git
git add .
git commit -m "Initial commit"
```

--------------

# systemd.service git-crit-files.service

```
[Unit]
Description=Watch admin files alert
#Documentation=

[Service]
Type=oneshot
ExecStart=/usr/local/bin/git-crit-files
```

# systemd.path git-crit-files.path

```
[Path]
PathChanged=/etc/fstab
PathChanged=/etc/hosts

[Install]
WantedBy=default.target
```

--------------

# Script /usr/local/bin/git-crit-files

```
#!/bin/bash

# TODO :
# - vérifier si existe dans /etc
# - vérifier si existe sur le dépôt

local_git_dir="/srv/.local-git"

#list_files="
#/etc/fstab
#/etc/hosts
#"

list_files=$(awk -F'=' '/PathChanged/ {print $2}' /etc/systemd/system/git-crit-files.path)

cd $local_git_dir

for file in $list_files ; do
    if ! diff -q $file ${file#/} ; then
        # LOG
        logger -t system-file-alert -p warning "File '$file' was changed"
        # Copie dans le depot
        cp $file $local_git_dir/$file
        # Commit
        git add $local_git_dir/$file
        git commit -m "Update $file
Last 10 ssh logs:
$(journalctl --identifier=ssh-wrapper --no-pager --lines=10)
"
    else
        logger -t system-file-alert -p debug "File '$file' NOT changed"
    fi
done
```

--------------

# sshrc (/etc/ssh/sshrc)

```
ip=`echo $SSH_CONNECTION | cut -d " " -f 1`

# Test if ip arealy present
if ! grep $ip /tmp/list_ip_ssh_$USER >/dev/null 2>&1
then
    #logger -t ssh-wrapper $USER login from $ip
    logger -t ssh-wrapper -p warning $USER login from unknown ip: $ip - $(host $ip|awk '{print $5}')
    #echo "User $USER just logged in from $ip - $(host $ip|awk '{print $5}')" |mail -s "New SSH Login to $USER in $(hostname)" admin-s3@zordhak.fr

    # add the ip in temporary list
    echo "$(date) - $ip" >> /tmp/list_ip_ssh_$USER
else
    logger -t ssh-wrapper -p info $USER login from known ip: $ip
fi
```

# Activation systemd.path

```
systemctl enable --now git-crit-files.path
```
