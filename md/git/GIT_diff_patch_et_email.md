% Git diff, Git patch et Git email

# Diff

+ Afficher les changements entre le dernier commit et les changements locaux
  ```bash
  git diff
  ```

+ Afficher les changements entre deux commit
  ```bash
  git diff tag1..tag2
  ```

----------------------------------------

# Git Patch

Le but du présent tuto est de faire un patch d'un coté, puis de l'appliquer ailleur.

Exemple, un serveur possède des clés de déploiement, mais pas de push possible, alors que l'admin a les droit pour.

## Création du patch

Sur le serveur, faire les modifications, add et commit, puis :

```bash
# Patch généré par rapport au dernier commit
git format-patch -o output HEAD^

# Patch généré entre plusieurs commit
# note, le patch est généré entre les deux ID, ce qui est dans ID_commit_debut n'est pas pris en compte
git format-patch <ID_commit_debut>..<ID_commit_fin>

-----

# Exemple - patch pour les commit 101 et 102
# Version n°3 du patch
# - commit 102 (intégré dans le patch)
# - commit 101 (intégré dans le patch)
# - commit 100 (non intégré dans le patch)
#
# Création des fichier suivants :
# - 0000-description
# - 0001-patch_commit_101
# - 0002-patch_commit_102
git format-patch --cover-letter -v3 -o output 100..102
```

+ `-k`, `--keep-subject` : Ne pas ajouter **[PATCH]** dans le sujet
+ `--stdout` : afficher la sortie dans la sortie standatd (stdout) au format mbox au lieu de créer un fichier de patch
+ `--cover-letter` : Ajouter un patch **0000** pour décrire les patch qui suivent (fichier à éditer ensuite)
+ `-o <dir>`, `--output-directory <dir>` : répertroire de sortie
+ `-v<version>` : spécifier la version du patch (dans le cadre d'une demande de modif après refus de fusion)

## Application du patch

Sur la station de l'admin, pour importer le patch avant de faire le push

```bash
# Méthode 1 - Application simple du patch en intégrant le commit (dont son numéro de commit)
git am file.patch

# Méthode 2 - Application du patch sans prendre en compte le commit
git apply mypatch.patch
```

+ `-k`, `--keep-subject` : Ne pas prendre en compte **[PATCH]** dans le sujet
+ `-3`, `--3way`, `--no-3way` : En cas d'échec, application du patch en 3 étapes (voir git-config)

----------------------------------------

# Git email

Envoyer le patch précédent créé via email

## Paquet requis
+ git
+ git-email

## Envoyer le mail

```
git send-email --from <expéditeur> --to <destinataire> --cc <copie> <patch_a_envoyer>
git send-email --from me@example.com --to dest@example.com --cc dest-cc@example.com output/000*

# Once your commits are ready to be sent to the mailing list, run the following commands:
# $ git format-patch --cover-letter -M origin/master -o outgoing/
# $ edit outgoing/0000-*
# $ git send-email outgoing/*
```

----------------------------------------

### Man à voir

+ man git-diff
+ man git-am
+ man git-apply
+ man git-config
+ [man git-mailinfo](https://git-scm.com/docs/git-mailinfo)
+ [man git format-patch](https://git-scm.com/docs/git-format-patch)
+ [man git send-mail](https://git-scm.com/docs/git-send-email)

