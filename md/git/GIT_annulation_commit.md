% Annuler des commit avec GIT

----------------------------------------

### Commit locaux (pas encore pushé)

+ Annuler le dernier commit local
```bash
git log
    commit 101: bad commit    # latest commit, this would be called 'HEAD'
    commit 100: good commit   # second to last commit, this is the one we want

# To restore everything back to the way it was prior to the last commit,
# we need to reset to the commit before HEAD:

git reset --soft HEAD^     # use --soft if you want to keep your changes
git reset --hard HEAD^     # use --hard if you don't care about keeping the changes you made

# Now git log will show that our last commit has been removed.
```

+ Annuler plusieurs commit locaux
```bash
git log
commit 102 : HS
commit 103 : HS
commit 104 : OK

# On veut annuler les commits jusqu'au 104
git reset --soft 104    # Annule les commit jusqu'au 104 mais garde les modifications
git reset --hard 104    # Annule les commit jusqu'au 104 ET SUPPRIME LES MODIFICATIONS
```

----------------------------------------

### Commit public
```bash
# If you have already made your commits public,
# you will want to create a new commit which will "revert"
# the changes you made in your previous commit (current HEAD).

git revert HEAD

# Your changes will now be reverted and ready for you to commit:

git commit -m 'restoring the file I removed by accident'
git log
    commit 102: restoring the file I removed by accident
    commit 101: removing a file we don't need
    commit 100: adding a file that we need
```

Plus d'info, voir :

[Git-Basics-Undoing-Things](https://git-scm.com/book/en/v2/Git-Basics-Undoing-Things)
