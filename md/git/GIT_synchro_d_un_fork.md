% GIT et la synchro d'un fork via le dépot amont

Nous avons :

+ 1 dépot d'origine : https://git.example.com/projet/depot.git
+ 1 dépôt de fork : https://git.example.com/my_name/depot.git

--------------------

+ On clone le dépôt de travail, le fork
```
git clone https://git.example.com/my_name/depot.git
```
+ On a fait des modifications... ou non
+ On veut synchroniser le dépôt amont (upstream) dans notre fork
+ Étapes :

1. Ajouter le dépôt upstream
```
git remote add upstream https://git.example.com/projet/depot.git
```

2. fetch
```
git fetch upstream
```

3. Au cas ou, on retourne dans la branche master
```
git checkout master
```

4. Deux solutions
  1. On merge la branche master upstream avec notre branche master
  ```
  git merge upstream/master
  # ou
  git merge upstream/master master
  ```
  2. On écrase la branche locale par la branche upstream
  ```
  git rebase upstream/master
  ```

6. Synchro du dépôt distant avec la branche master mise à jour
```
git push origin master
```
