% GIT - Options de clone

## Cloner juste une branche
```bash
git clone --single-branch --branch social_contract_french https://salsa.debian.org/albanvidal-guest/debian-flyers.git
```

+ `--branch <branche>` : indique la branche que l'on souhaite cloner
+ `--single-branch` : clone seulement la branche sélectionnée

## Clone partiel (shallow clone)
```bash
# Cloner qu'un certain nombre de commit
git clone --depth 2 https://salsa.debian.org/debian/debian-flyers.git

# Cloner après une date
git clone --shallow-since=2019-02-01 https://salsa.debian.org/debian/debian-flyers.git
```

+ `--depth <nombre>` : spécifie le nombre de commit que l'on veut synchroniser (shallow clone)
+ `--shallow-since=<date>` : clone partiel après la date (format **AAAA-MM-JJ**)
