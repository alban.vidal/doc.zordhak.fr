% Check NRPE

### Paquet à installer

+ nagios-nrpe-plugin

### Commande de check

```bash
/usr/lib/nagios/plugins/check_nrpe -H <FQDN> -c <CHECK>
/usr/lib/nagios/plugins/check_nrpe -H <FQDN> -c check_reboot
```

