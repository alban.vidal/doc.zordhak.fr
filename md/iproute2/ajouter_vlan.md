% Ajouter un VLAN taggé avec iproute2

```
ip link add link <NOM_INTERFACE_PHYSIQUE> name <NOM_NOUVELLE_INTERFACE> type vlan id <ID>
```

```
# Exemple
ip link add link eth0 name vlan2 type vlan id 2

ip link set vlan2 up
ip a a 10.10.10.10/24 dev vlan2
```
