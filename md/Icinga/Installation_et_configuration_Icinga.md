% Installation et configuration Icinga

# ATTENTION - BROUILLON

# Prérequis

+ Installation DEB 9 de base
+ dépôts
+ clé RSA
+ tunning vim

--------------------------------------------------------------------------------

# Installation de Icinga et de la base de donnée :

apt-get update
apt-get -y install vim icinga2 nagios-plugins vim-icinga2 mariadb-server mariadb-client icinga2-ido-mysql apache2 php-gettext php-curl

# plugins en plus : nagios-plugins-contrib
# PAS INSTALLÉ # voir si besoin de zend, par exemple php-zend-code ou autre...
# NOTE pendant installation
icinga2-ido-mysql debconf : OUI - OUI - MDP_IDO_SQL

--------------------------------------------------------------------------------

# Configuration de la colorisation syntaxique de VIM
vim-addon-manager -w install icinga2

--------------------------------------------------------------------------------

# Activer le module « MySQL » :
icinga2 feature enable ido-mysql
# Activer le module « command » pour lancer des commandes en CLI
icinga2 feature enable command

# Mettre www-data dans le groupe icingacmd pour pouvoir lancer des commandes depuis apache
usermod -a -G nagios www-data
# vérif :
id www-data

# Redémarrer icinga pour prendre en compte le module activé :
systemctl restart icinga2.service

--------------------------------------------------------------------------------

####
# Parie Interface Web :
apt-get -y install icingaweb2

# Dans PHP modifier le timezone :
```
sed -i 's#^;date.timezone.*$#date.timezone = "Europe/Paris"#' /etc/php/7.0/apache2/php.ini
```

# VOIR CI NESESSAIRE LORS DE LA VERIF DE LA CONFIG
# redémarrer apache pour prise en compte :
systemctl restart apache2

# Désactiver le module pagespeed pour icinga si il est activé sur la machine, ajouter dans le vHost :
# /etc/apache2/conf-enabled/icingaweb2.conf ou dans le vhost ??
```
        <IfModule pagespeed_module>
                ModPagespeedDisallow "*/icingaweb2/*"
        </IfModule>
```

# Générer le token pour l'accès à la configuration web
```
icingacli setup token create
The newly generated setup token is: e9a1acd77f5428bf
```

# Si on a perdu le token, pour l'afficher :
```
icingacli setup token show
```

# Créer la BDD des utilisateurs IcingaWeb :
## création du mot de passe pour le compte admin Web
```
openssl passwd -1 MDP_ADMIN_WEB
##
mysql -u root
mysql> CREATE DATABASE icingaweb2;
mysql> GRANT SELECT, INSERT, UPDATE, DELETE, DROP, CREATE VIEW, INDEX, EXECUTE ON icingaweb2.* TO 'icingaweb2'@'localhost' IDENTIFIED BY 'MDP_WEB_SQL';
mysql> use icingaweb2;
mysql> INSERT INTO icingaweb_user (name,active,password_hash) VALUES ('icingaadmin',1,'HASH_MDP_ADMIN_WEB');
mysql> quit
```

## populer la base (VOIR SI C'EST AU BON ENDROIT !) :
mysql -p icingaweb2 < /usr/share/icingaweb2/etc/schema/mysql.schema.sql


# Configurer icinga via l'interface Web :
- ouvrir le navigateur avec l'ip du serveur, et ajouter : /icingaweb2/setup
- entrer le token > next
- cocher DOC et Monitoring > next
- vérification des prérequis (tout doit être vert sauf PostgreSQL) > next
- authentification : Database > next
- création de la base de donnée utilisateurs
  -- Resource Name : icingaweb2
  -- Database Type : MySQL
  -- Host : localhost
  -- Port : 3306
  -- Database Name : icingaweb2
  -- Username : icingaweb2
  -- Password : MDP_WEB_SQL
  -- Character Set : utf8
  > Validate Configuration
  > Next
- Configuration de la base : Il faut lui dire avec quel utilisateur de la base on va pouvoir créer la base configuré juste avant, donc :
  -- Username : icingaweb2
  -- Password : MDP_WEB_SQL
- Backend Name : icingaweb2 > next
- Admin :
  -- Username : icingaadmin
  -- Password : MDP_ADMIN_WEB
  -- repet : MDP_ADMIN_WEB
- App config > next
- récap > next
- welcome... > next
- monitoring backend :
  -- name : icinga
  -- type IDO
  > next
- Monitoring IDO
  -- Name : icinga2
  -- Port 3306
  -- Database Name : icinga2
  -- Username : icinga2
  -- Password : MDP_IDO_SQL
  > next
- Command Transport > next
- Monitoring security > next
- FINISH



############## NOTES

/etc/dbconfig-common/icinga2-ido-mysql.conf => dbc_dbpass=PASSWORD
/etc/icinga2/features-available/ido-mysql.conf => password

/etc/icingaweb2/config.ini => Fichier de conf
/etc/icingaweb2/authentication.ini => auth config

### LISTER LES MODULES icinga2 :
icinga2 feature list


# VOIR
nagios-plugins/stable,now 2.2-3 all  [installé]
  transitional dummy package (nagios-plugins to monitoring-plugins)

nagios-plugins-basic/stable 2.2-3 all
  transitional dummy package

nagios-plugins-common/stable 2.2-3 all
  transitional dummy package

nagios-plugins-contrib/stable 21.20170222 amd64
  Plugins for nagios compatible monitoring systems

nagios-plugins-standard/stable 2.2-3 all
  transitional dummy package


