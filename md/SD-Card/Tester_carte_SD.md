% Tester une carte SD

Utilitaire à utiliser : **f3**

--------------------

Installation :

```
apt install f3
```

--------------------

Test d'écriture :

```
sudo f3write /mnt/carte-sd
```

--------------------

Test de lecture

```
sudo f3read /mnt/carte-sd
```
