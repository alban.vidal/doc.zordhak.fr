% Modifier le port ssh pour GitLab

## Copier le fichier de configuration original

```bash
cp /etc/ssh/sshd_config /etc/ssh/sshd_config-gitlab
```

## Modifier le port d'écoute

```bash
# À éditer dans le fichier /etc/ssh/sshd_config-gitlab
Port 1234
```

## N'autoriser que l'utilisateur GitLab

```bash
# À éditer dans le fichier /etc/ssh/sshd_config-gitlab
AllowUsers gitlab
```

## Copie du fichier de service original

```bash
cp /lib/systemd/system/ssh.service /etc/systemd/system/sshd-gitlab.service
```

## Modifier la desciption

```bash
# À éditer dans le fichier /etc/systemd/system/sshd-gitlab.service
Description=SSH Daemon for GitLab
```

## Changer la commande exécutée

```bash
# À éditer dans le fichier /etc/systemd/system/sshd-gitlab.service
ExecStart=/usr/sbin/sshd -D -f /etc/ssh/sshd_config-gitlab $OPTIONS
```

## Supprimer les lignes inutiles

```bash
sed -i                                        \
    -e '/Alias=sshd.service/d'                \
    -e '/ExecReload=\/usr\/sbin\/sshd -t/d'   \
    -e '/ExecStartPre=\/usr\/sbin\/sshd -t/d' \
    /etc/systemd/system/sshd-gitlab.service
```

## Recharger les démons

```bash
systemctl daemon-reload
```

## Activtion et démarrage du nouveau démon

```bash
systemctl enable --now sshd-gitlab.service
```

## Configuration du port d'écoute dans GitLab

```bash
gitlab_rails['gitlab_shell_ssh_port'] = 2222
```

## Reconfigurer gitlab

```bash
gitlab-ctl reconfigure
```
