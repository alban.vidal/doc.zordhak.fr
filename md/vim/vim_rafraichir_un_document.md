% VIM - Rafraichir un document

```bash
# Sans option recharge le document courant
:edit

# Si on a fait des modifications que nous souhaitons écraser
:edit!

# Abréviation
:e
:e!
```
