% VIM - L'incrémentation

Exemple de la ligne à copier :

```
Ligne 1
```

----------------------------------------

# Méthode 1

+ copier jusqu'à la fin du fichier
+ coller 20 fois
+ tout sélectionner
+ tout incrémenter

```
yy 20p vG$ g ^a
```

----------------------------------------

# Méthode 2 (macro)

+ commencer la macro
+ copier
+ coller
+ incrémenter
+ fin macro
+ répéter 20 fois la macro

```
qq Y p ^a q 20 @a
```
