% VIM et les options au lancement

Extrait du man vim

--------------------------------------------------------------------------------

+ `vim dummy +[num]`

Place le curseur sur la ligne "num" dans le premier fichier. Si "num" est omis, le curseur sera placé sur la dernière ligne.

--------------------------------------------------------------------------------

+ `vim dummy +/{motif}`

Place le curseur sur la première occurrence de {motif} dans le premier fichier. Voir ":help search-pattern" pour connaître les motifs de recherches disponibles.

--------------------------------------------------------------------------------
