% VIM : Basculer en majuscule / minuscule

Exemple de contenu de fichier :

```
Nom prénom téléphone
Xxx xxx xxx
```

+ Convertir la ligne en majuscule :
```
V gU
```

+ Convertir la ligne en minuscule :
```
V gu
```
