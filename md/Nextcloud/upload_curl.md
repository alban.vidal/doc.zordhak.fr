% Nextcloud et l'envoi de fichier au travers de curl

----------------------------------------

### Création du partage

- Créer un répertoire
- Partager en mode « Dépôt de fichier (téléversement uniquement) »
- Modifier le lien en « upload » (nécessite l'application [ShareRenamer](https://apps.nextcloud.com/apps/sharerenamer))

----------------------------------------

### Envoyer un fichier

```
# Utilisation :
curl --user <NOM_DU_PARTAGE>: --upload-file fichier.txt https://cloud.example.com/public.php/webdav/

# Exemple :
curl --user upload: --upload-file fichier.txt https://cloud.example.com/public.php/webdav/
```

----------------------------------------

### Modifier le nom du fichier envoyé

```
# Le nom du fichier sera azerty.txt
curl --upload-file fichier.txt --user upload: https://cloud.example.com/public.php/webdav/azerty.txt
```

----------------------------------------

### Envoyer du texte dans le fichier azerty.txt

```
curl --user upload: -X PUT --data "Hello Wolrd !"  https://cloud.example.com/public.php/webdav/azerty.txt
```

