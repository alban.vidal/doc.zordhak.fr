% Nextcloud and upload via curl

----------------------------------------

### Create shared directory

- Create a directory
- Share it in 'Upload' mode
- Edit the link to 'upload' (need app [ShareRenamer](https://apps.nextcloud.com/apps/sharerenamer))

----------------------------------------

### Send file

```
# Usage :
curl --user <SHARED_NAME>: --upload-file file.txt https://cloud.example.com/public.php/webdav/

# Example :
curl --user upload: --upload-file file.txt https://cloud.example.com/public.php/webdav/
```

----------------------------------------

### Change the name of uploaded file

```
# Le nom du fichier sera azerty.txt
curl --upload-file fichier.txt --user upload: https://cloud.example.com/public.php/webdav/azerty.txt
```

----------------------------------------

### Send some text in 'azerty.txt' file

```
curl --user upload: -X PUT --data "Hello Wolrd !"  https://cloud.example.com/public.php/webdav/azerty.txt
```

