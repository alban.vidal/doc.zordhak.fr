% RAM et SWAP

## Vider le cache de la RAM

```
sync && echo 3 > /proc/sys/vm/drop_caches
```

## Trier les process par l'utilisation du SWAP

Colones : NOM, PID, Taille de swap utilité (en kB)

```
for file in /proc/*/status ; do awk '/Tgid|VmSwap|Name/{printf $2 " " $3}END{ print ""}' $file; done | grep kB  | sort -k 3 -n
```
