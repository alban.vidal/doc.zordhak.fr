% Apache et les tests de performance

**Apache HTTP server benchmarking tool**

### Paquet à installer

```bash
sudo apt install apache2-utils
```

### Lancer un test
```bash
# -k Use HTTP KeepAlive feature
# -c concurrency  Number of multiple requests to make at a time
# -n requests     Number of requests to perform
ab -k -c 350 -n 20000 example.com/
```
