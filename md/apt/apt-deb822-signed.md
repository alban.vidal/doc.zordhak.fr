% apt Deb822 et les signatures

Exemple avec le paquet **codium**

### Télécharger la clé GPG du dépôt

```bash
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/vscodium.gpg > /dev/null
```

### Créer le fichier de sources pour apt

```bash
echo '
Types: deb
URIs: https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs
Suites: vscodium
Components: main
Signed-By: /usr/share/keyrings/vscodium.gpg
' | sudo tee /etc/apt/sources.list.d/vscodium.sources
```

### Rafraichissement des dépôt et installation

```bash
sudo apt update
sudo apt install codium
```
