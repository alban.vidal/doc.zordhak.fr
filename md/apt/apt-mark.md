% apt-mark

### Afficher les paquets installés automatiquement ou manuellement

```bash
# Affiche les paquets installés automatiquement
apt-mark showauto

# Affiche les paquets installés manuellement
apt-mark showmanual

# Passer un paquet de manuel à auto
apt-mark auto PAQUET

# Passer un paquet de auto à manuel
apt-mark manual PAQUET
```

### Bloquet un paquet (pour ne pas le mettre à jour)

```bash
# Afficher les paquets bloqués
apt-mark showhold

# Bloquer un paquet
apt-mark hold tzdata

# Débloquer un paquet
apt-mark unhold tzdata
```

### Fichier contenant les paquets auto-installés

`/var/lib/apt/extended_states`
