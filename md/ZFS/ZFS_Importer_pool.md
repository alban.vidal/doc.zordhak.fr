% ZFS - Importer un pool

Lister les pools actuellement montés  
`zpool list`

Lister les pools importables  
`zpool import`

Listes les pools supprimés  
`zpool import -D`

Importer un pool  
`zpool import <NOM|ID>`

Importer tous les pools disponibles  
`zpool import -a`

Importer un pool sans monter les volumes  
`zpool import -N <NOM|ID>`

Importer un pool en changeant la racine sur */mnt*  
`zpool import -R /mnt <NOM|ID>`

Importer un pool en le renommant temporairement (exemple : nom déjà existant)  
`zpool import -R /mnt -t <ID> <NOM_Temporaire>`
