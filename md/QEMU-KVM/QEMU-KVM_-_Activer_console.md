% QEMU - KVM - Activer la console série

--------------------------------------------------------------------------------

Sur le système invité :

```
systemctl enable serial-getty@ttyS0.service
systemctl start serial-getty@ttyS0.service
```

Depuis l'hôte

```
virsh console ma-vm
```

--------------------------------------------------------------------------------

Pour activer la console après grub, ajouter la ligne suivante dans le fichier `/etc/default/grub`

```
GRUB_CMDLINE_LINUX="console=ttyS0"
# ou
GRUB_CMDLINE_LINUX="console=tty0 console=ttyS0,9600n8"
```

Mettre à jour grub et rebooter

```
update-grub

reboot
```
