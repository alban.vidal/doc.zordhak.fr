% QEMU - KVM - Réduire la taille d'une image

--------------------------------------------------------------------------------

Après la copie d'une image ou de la convertion d'un pc physique en VM,
il peut être utile de réduire la taille de l'image sur le disque avec le **thin-provisioning**.

--------------------------------------------------------------------------------

## Paquets nécessaires

+ qemu-utils : pour qemu-img
+ libguestfs-tools : pour virt-sparsify

```
apt install libguestfs-tools qemu-utils
```

--------------------------------------------------------------------------------

## Afficher les informations de l'image

```
qemu-img info IMAGE.qcow2
```

## Si on désire convertire un image brute en qcow2

Par exemple, si cela provient d'un `dd` pour en faire une VM

```
qemu-img convert -p -f raw Win7.qcow2 -O qcow2 Win7-QCOW2.qcow2
```

## Réduire la taille libre de l'image en taille libre sur le disque

```
virt-sparsify -v SOURCE.qcow2 DEST-sparsi.qcow2

# ou sur l'image elle-même
virt-sparsify -v -in-place SOURCE.qcow2
```

