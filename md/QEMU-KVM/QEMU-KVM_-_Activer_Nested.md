% KVM - Activation du Nested

--------------------------------------------------------------------------------

## Intel

### Savoir si activé (Intel)

```
cat /sys/module/kvm_intel/parameters/nested
```

### Activer Nested (Intel)

```
cat << EOF > /etc/modprobe.d/kvm-nested-intel.conf
options kvm-intel nested=1
options kvm-intel enable_shadow_vmcs=1
options kvm-intel enable_apicv=1
options kvm-intel ept=1
EOF

# stopper les VM et recharger le module
modprobe -r kvm_intel
modprobe -a kvm_intel
```

--------------------------------------------------------------------------------

## AMD

### Savoir si activé (ADM)

```
cat /sys/module/kvm_amd/parameters/nested
```

### Activer Nested (AMD)

```
rmmod kvm-amd
echo 'options kvm-amd nested=1' >> /etc/modprobe.d/kvm-nested-amd.conf
modprobe kvm-amd
```

--------------------------------------------------------------------------------

## Vérifier la conf KVM

```
virt-host-validate
```

--------------------------------------------------------------------------------

## Conf de la VM

```
virsh dumpxml nested-kvm|grep 'cpu mode'
#  <cpu mode='host-passthrough'/>
#ou
#  <cpu mode='host-model'/>
```

--------------------------------------------------------------------------------

## Dans une VM, savoir si activé

```
lsmod | grep kvm
#kvm_intel             170200  0
#kvm                   566604  1 kvm_intel
#irqbypass              13503  1 kvm

lscpu | grep -E 'Virtualization|Hypervisor'
#Virtualization:      VT-x
#Hypervisor vendor:   KVM
#Virtualization type: full
```

--------------------------------------------------------------------------------

## Liens

+ [KVM - Nested Guests](https://www.linux-kvm.org/page/Nested_Guests)
