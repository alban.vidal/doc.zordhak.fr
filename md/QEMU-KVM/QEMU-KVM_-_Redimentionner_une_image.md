% Redimentionner une image qcow2

--------------------

# Méthode propre

paquet : libguestfs-tools

```
virt-resize <commande à compléter>
```

--------------------

# Méthode brute
```
qemu-img resize <my_vm>.img +10G
```

--------------------

# Avoir des infos sur une image
```
qemu-img info /var/lib/libvirt/images/sles12sp2-1.qcow2
```

