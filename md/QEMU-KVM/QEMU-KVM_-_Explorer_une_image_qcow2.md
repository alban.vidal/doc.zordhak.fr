% Explorer une image qcow2

----------------------------------------

# Méthode 1 : guestmount

## Prérequis pour guestmount

Le paquet libguestfs-tools  est nécessaire, sous Debian pour l'installer :
```
apt install libguestfs-tools
```

## Utilisation

### Montage
```
guestmount -a /chemin/vers/image.qcow2 -m <périphérique> /chemin/vers/point/de/montage
```
Note : ajouter l'option « -r » si on désire monter en lecture seule

L'option -m <périphérique> désigne la partition dans l'image qcow2

### Découverte des partitions dans une image

Si on ne connait pas le nom des périphériques présents, on peut utiliser la commande suivante pour les lister :
```
virt-filesystems -a /chemin/vers/image.qcow2
```

## Exemple

### Découverte de la partition dans l'image (système avec lvm)
```
virt-filesystems -a test.qcow2
(...)
/dev/vg_sys/lv_root
(...)
```

### Montage de la partition
```
guestmount -a test.qcow2 -m /dev/vg_sys/lv_root /srv/test
ls /srv/test
bin   dev  home        initrd.img.old  lib64       media  opt   root  sbin  sys  usr  vmlinuz
boot  etc  initrd.img  lib             lost+found  mnt    proc  run   srv   tmp  var  vmlinuz.old
```

### Démonter la partition

Pour démonter c'est très simple, si suffit d'utiliser la commande guestunmount
```
guestunmount /srv/test
```
----------------------------------------

# Méthode 2 : qemu-nbd

qemu-nbd - QEMU Disk Network Block Device Server

## Prérequis pour qemu-nbd

Le paquet qemu-utils  est nécessaire, sous Debian pour l'installer :
```
apt install qemu-utils
```

## Utilisation

### Il est d'abord nécessaire de charger le module nbd avec l'option max_part à 8
```
modprobe nbd max_part=8
```

### Attacher l'image sur un périphérique nbd
```
qemu-nbd --connect=/dev/nbd0 /chemin/vers/image.qcow2
```
Note : ajouter l'option « -r » si on désire monter en lecture seule

### Vérifier si l'image est bien chargée par nbd

```
dmesg | grep nbd0
```

### Lister les partitions présentes dans l'image
```
fdisk -l /dev/nbd0|grep ^/dev
```

### Démonter un périphérique nbd
```
qemu-nbd --disconnect /dev/nbd0
```

## Exemple

```shell
# Attacher l'image à nbd
qemu-nbd --connect=/dev/nbd0 /mnt/images/d9-c.qcow2

# Vérification
dmesg | grep nbd0

# Lister les partitions
fdisk -l /dev/nbd0|grep ^/dev

# Monter une partition
mount /dev/nbd0p2 /srv/test

(...ce quon a a faire dans la partition...)

# Démonter la partition
umount /dev/nbd0p2

# Détacher l'image de nbd
qemu-nbd --disconnect /dev/nbd0

# Décharger le module nbd
modprobe -r nbd
```
