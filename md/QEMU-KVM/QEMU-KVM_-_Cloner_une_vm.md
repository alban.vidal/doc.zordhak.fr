% KVM - Cloner une VM

## Commande de clone

```bash
virt-clone --original {original_VM} --name {new_VM} --auto-clone
```

--------------------------------------------------------------------------------

## Exemple

--------------------------------------------------------------------------------

### On liste les VM existantes

```bash
virsh list --all
# Id    Name                           State
#----------------------------------------------------
# -     debian10-template              shut off
```

--------------------------------------------------------------------------------

### On clone la VM « template » en « test »

```bash
virt-clone --original debian10-template --name deb10-test --auto-clone
```

--------------------------------------------------------------------------------

### Résultat

```bash
virsh list --all
# Id    Name                           State
#----------------------------------------------------
# -     debian10-template              shut off
# -     deb10-test                     shut off
```

--------------------------------------------------------------------------------

### Tuner la VM une fois le clône fait

#### Booter la nouvelle VM

```bash
virsh start deb10-test
```

#### La renommer

**à faire dans la VM**

```bash
hostnamectl set-hostname deb10-test

# NOTE : penser à aussi mettre le couple IP/NOM dans le fichier /etc/hosts
```

#### Regénérer les ID systemd et dbus

**à faire dans la VM**

```bash
# Systemd
rm /etc/machine-id
systemd-machine-id-setup

# dbus
rm /var/lib/dbus/machine-id
dbus-uuidgen --ensure
```

#### Regénérer les clés de serveur ssh

**à faire dans la VM**

```bash
# Suppression des anciennes clés
rm -v /etc/ssh/ssh_host_*

# Génération des nouvelles
dpkg-reconfigure openssh-server
```

#### Regénérer l'identifiant popularity-contest

**à faire dans la VM**

Seulement si popularity-contest est utilisé.

Voir https://neuro.debian.net/popularity.html

```bash
sed -i \
    -e 's,PARTICIPATE *= *.no.,PARTICIPATE="yes",g' \
    -e '/^ *MY_HOSTID/d' \
    /etc/popularity-contest.conf

DEBIAN_FRONTEND=noninteractive dpkg-reconfigure popularity-contest
```

--------------------------------------------------------------------------------
