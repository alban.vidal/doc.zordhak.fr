% Augmenter la taille d'un pool BTRFS

## NE FONCTIONNE PAS !!

```bash

# Voir si besoin
truncate -s +5G /var/snap/lxd/common/lxd/disks/default.img

# Lister les FS
btrfs filesystem show

#######################

mount /dev/loop3 /mnt

# voir lequel est OK
btrfs filesystem resize max /mnt/
# ou
# NOTE : « 1: » = devid récupéré avec « btrfs filesystem show »
btrfs filesystem resize 1:max /mnt
```


## Autre

```bash

# DF
# Il faut avoir monté le FS
btrfs filesystem df /mnt

# Info FS
btrfs filesystem show /mnt
# ou
btrfs filesystem show /dev/loop3
```
