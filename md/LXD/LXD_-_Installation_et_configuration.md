% LXD - Installation et configuration

-----------------------------------------------------------------------------------------------------------------------

# Configuration Réseau

-----------------------------------------------------------------------------------------------------------------------

Configurer un bridge sur l'isolateur
Dans ce modèle il faut avoir « br0 » de fonctionnel

```
apt install bridge-utils
```

-----------------------------------------------------------------------------------------------------------------------

# Installation de LXD

-----------------------------------------------------------------------------------------------------------------------

## Installation depuis snap

```
apt install snapd

snap install core

snap install lxd

exit
```

**reconnexion à l'isolateur (obligé car l'os n'est pas au courant que le snap a lancé des services... bug)**

-----------------------------------------------------------------------------------------------------------------------

## Installation de LXD depuis les sources

https://github.com/lxc/lxd/blob/master/README.md

```
apt update

apt install acl dnsmasq-base git golang liblxc1 lxc-dev libacl1-dev make pkg-config rsync squashfs-tools tar xz-utils
```

**Outils LVM et thin LVM**

```
apt install lvm2 thin-provisioning-tools
```

**To run the testsuite, you'll also need:**

```
apt install curl gettext jq sqlite3 uuid-runtime bzr
```

## Building the tools

```
mkdir -p /opt/go

export GOPATH=/opt/go
```

-----------------------------------------------------------------------------------------------------------------------

### SI ON EST EN LIGNE :

```
go get github.com/lxc/lxd
```

[comment]: <> ### SI ON EST HORS LIGNE, télécharger le dépôt, le copier sur l'hôte, et exécuter GO
[comment]: <> 
[comment]: <> ** depuis un autre pc qui a téléchargé les sources **
[comment]: <> 
[comment]: <> ```
[comment]: <> scp -r /opt/go/src 192.168.3.13:/opt/go/
[comment]: <> ```

```
go install github.com/lxc/lxd/
```

-----------------------------------------------------------------------------------------------------------------------

### Compilation

```
cd $GOPATH/src/github.com/lxc/lxd

make
```

-----------------------------------------------------------------------------------------------------------------------

## Création du service LXD

```
cat << EOF > /etc/systemd/system/lxd.service
[Unit]
Description=LXD
After=network.target

[Service]
ExecStart=/opt/go/bin/lxd
ExecReload=/bin/kill -HUP $MAINPID
KillMode=process
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF
```

```
systemctl enable lxd

systemctl start lxd
```

-----------------------------------------------------------------------------------------------------------------------

## Création des liens

```
ln -s /opt/go/bin/lxd /usr/local/bin/

ln -s /opt/go/bin/lxc /usr/local/bin/

ln -s /opt/go/bin/lxd-benchmark /usr/local/bin/

ln -s /opt/go/src/github.com/lxc/lxd/config/bash/lxd-client /etc/bash_completion.d/
```

-----------------------------------------------------------------------------------------------------------------------

# Configuration

-----------------------------------------------------------------------------------------------------------------------

## Initialisation LXD

### Initialisation Auto :

```
cat << EOF | lxd init --preseed
config:
  core.https_address: :8443
  images.auto_update_interval: 15
# Profiles
profiles:
- name: default
  config:
    limits.memory: 1GB
EOF
```

#### Mot de passe pour la connexion à distance

```
lxc config set core.trust_password <something-secure_PASSWD>
```

### Initialisation à la main :

```
lxd init
```

[comment]: <> #  - yes (Do you want to configure a new storage pool (yes/no) [default=yes]?)
[comment]: <> #  - lxd_lvm (Name of the new storage pool [default=default]: lxd_lvm)
[comment]: <> #  - lvm (Name of the storage backend to use (dir, lvm) [default=dir]: lvm)
[comment]: <> #  - yes (Create a new LVM pool (yes/no) [default=yes]?)
[comment]: <> #  - yes (Would you like to use an existing block device (yes/no) [default=no]? yes)
[comment]: <> #  - /dev/vdc (Path to the existing block device: /dev/vdc)
[comment]: <> #  - yes (Would you like LXD to be available over the network (yes/no) [default=no]?)
[comment]: <> #  - all (Address to bind LXD to (not including port) [default=all]:)
[comment]: <> #  - 8443 -Port to bind LXD to [default=8443]:)
[comment]: <> #  - toor  (Trust password for new clients:)
[comment]: <> #  - yes (Would you like stale cached images to be updated automatically (yes/no) [default=yes]?)                       - no (Would you like to create a new network bridge (yes/no) [default=yes]?)
[comment]: <> #//  - yes (Would you like to create a new network bridge (yes/no) [default=yes]?)
[comment]: <> #//  -lxdbr0 ( What should the new bridge be called [default=lxdbr0]?)
[comment]: <> #//  - auto (What IPv4 address should be used (CIDR subnet notation, “auto” or “none”) [default=auto]?)
[comment]: <> #//  - none (What IPv6 address should be used (CIDR subnet notation, “auto” or “none”) [default=auto]? none)
[comment]: <> #```

-----------------------------------------------------------------------------------------------------------------------

## Configuration réseau

On attache le bridge avec la carte eth0 qui sera dans le profile **default**

```
lxc network attach-profile br0 default eth0

lxc network list
```

-----------------------------------------------------------------------------------------------------------------------

## Limitation CPU et mémoire dans le profil par défaut

```
lxc profile set default limits.cpu 1
lxc profile set default limits.memory 512MB
```

[comment]: <> ne peut pas être fait ici, on a pas encore le backend LVM
[comment]: <> lxc profile device set default root size 4GB

-----------------------------------------------------------------------------------------------------------------------

## Creation du profile fast (4 core, 4GB de RAM)

```
lxc profile create fast
lxc profile list
lxc profile set fast limits.cpu 4
lxc profile set fast limits.memory 4GB
lxc profile show fast
```

-----------------------------------------------------------------------------------------------------------------------

## Création du LV Thin et configuration du stockage

Doc https://github.com/lxc/lxd/blob/master/doc/storage.md

```
pvcreate /chemin/vers/perif/bloc
vgcreate vg_lxd /chemin/vers/perif/bloc
lvcreate -L 50G -T vg_lxd/LXDThinpool

lxc storage create lvm lvm 
lxc storage set lvm lvm.vg_name vg_lxd
lxc storage set lvm lvm.thinpool_name LXDThinpool
```

### Puis configurer le profile default pour utiliser ce LV :

```
lxc profile device add default root disk path=/ pool=lvm
lxc profile device set default root size 2GB
lxc profile show default
```

-----------------------------------------------------------------------------------------------------------------------

## Machine Setup

**You'll need sub{u,g}ids for root, so that LXD can create the unprivileged containers:**

```
echo "root:1000000:65536" | tee -a /etc/subuid /etc/subgid
```

(reboot nécessaire pour prise en compte)

-----------------------------------------------------------------------------------------------------------------------

## Ajouter les hôtes distants 

A FAIRE SUR TOUS LES HÔTES !

```
lxc remote add hote-1 https://192.168.x.y:8443
lxc remote add hote-2 https://192.168.x.z:8443

lxc remote list
```

-----------------------------------------------------------------------------------------------------------------------

## CRIU (checkpoint and restore in userspace)

### Ajout du dépôt SID

```
cat << EOF > /etc/apt/sources.list.d/sid.list
deb http://ftp.fr.debian.org/debian/ sid main
EOF

cat << EOF > /etc/apt/preferences.d/sid
Package: *
Pin: release a=unstable
Pin-Priority: 50
EOF
```

### Installation du paquet crui

```
apt update && apt install criu
```


-----------------------------------------------------------------------------------------------------------------------

# Liens utiles

-----------------------------------------------------------------------------------------------------------------------

## LXD : Git et Doc officielles

https://github.com/lxc/lxd

https://github.com/lxc/lxd/tree/master/doc

https://linuxcontainers.org/lxd/

----------------------------------------------------------------

## Blog du Leader du projet LXD (Stéphane Graber)

### 12 étapes d'installation de d'utilisation

LXD 2.0: Blog post series [0/12]
https://stgraber.org/2016/03/11/lxd-2-0-blog-post-series-012/

LXD 2.0: Introduction to LXD [1/12]
https://stgraber.org/2016/03/11/lxd-2-0-introduction-to-lxd-112/

LXD 2.0: Installing and configuring LXD [2/12]
https://stgraber.org/2016/03/15/lxd-2-0-installing-and-configuring-lxd-212/

LXD 2.0: Your first LXD container [3/12]
https://stgraber.org/2016/03/19/lxd-2-0-your-first-lxd-container-312/

LXD 2.0: Resource control [4/12]
https://stgraber.org/2016/03/26/lxd-2-0-resource-control-412/

LXD 2.0: Image management [5/12]
https://stgraber.org/2016/03/30/lxd-2-0-image-management-512/

LXD 2.0: Remote hosts and container migration [6/12]
https://stgraber.org/2016/04/12/lxd-2-0-remote-hosts-and-container-migration-612/

LXD 2.0: Docker in LXD [7/12]
https://stgraber.org/2016/04/13/lxd-2-0-docker-in-lxd-712/

LXD 2.0: LXD in LXD [8/12]
https://stgraber.org/2016/04/14/lxd-2-0-lxd-in-lxd-812/

LXD 2.0: Live migration [9/12]
https://stgraber.org/2016/04/25/lxd-2-0-live-migration-912/

LXD 2.0: LXD and Juju [10/12]
https://stgraber.org/2016/06/06/lxd-2-0-lxd-and-juju-1012/

LXD 2.0: LXD and OpenStack [11/12]
https://stgraber.org/2016/10/26/lxd-2-0-lxd-and-openstack-1112/

LXD 2.0: Debugging and contributing to LXD [12/12]
https://stgraber.org/2017/02/27/lxd-2-0-debugging-and-contributing-to-lxd-1212/

-----------------------------------------------------------------

### Autres articles

Network management with LXD (2.3+)
https://stgraber.org/2016/10/27/network-management-with-lxd-2-3/

Running snaps in LXD containers
https://stgraber.org/2016/12/07/running-snaps-in-lxd-containers/

Running Kubernetes inside LXD
https://stgraber.org/2017/01/13/kubernetes-inside-lxd/

----------------------------------------------------------------

### Vidéo de démonstation par Stéphane Graber (Debconf17)

https://debconf17.debconf.org/talks/53/

----------------------------------------------------------------


----------------------------------------

# ~ Utile ~

#### Afficher les details sur les stockages et les volumes lies :
```
lxc storage list
lxc storage show <nom_storage>
lxc storage volume list <nom_storage>
```

#### creer un CT sans le lancer
#### Creating a container without starting it
```
lxc init images:debian/stretch d9-power-off
```

#### premier CT (télécharge depuis le net)
```
lxc launch images:debian/stretch d9-1
lxc list
lxc image list (liste les images locales)
lxc image list images: (liste les images du net dispo)
```

#### Getting detailed information from a container
```
lxc info <container>
```

#### start / stop ...
```
lxc start <container>
lxc stop <container>
lxc stop <container> --force
lxc restart <container>
lxc restart <container> --force
lxc pause <container>
lxc delete <container>
```

#### Configuration profiles

**The list of all available profiles can be obtained with:**
```
lxc profile list
```

**To see the content of a given profile, the easiest is to use:**
```
lxc profile show <profile>
```

**And should you want to change anything inside it, use:**
```
lxc profile edit <profile>
```

**You can change the list of profiles which apply to a given container with:**
```
lxc profile apply <container> <profile1>,<profile2>,<profile3>,...
```

-------------------------------------

#### INFO

 + Lister les profiles
     lxc profile list
 + Détail d'un profile
     lxc profile show default
