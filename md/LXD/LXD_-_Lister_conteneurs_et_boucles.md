% Différents trucs et astuces

### Lister tous les conteneurs
```bash
lxc query /1.0/containers | jq '.[]'|sed 's#.*containers/\(.*\)"#\1#'
```

### Lister les conteneurs allumée

```bash
# Présentation propre - Attention, c'est plus long !
lxc ls volatile.last_state.power=RUNNING

# Juste les noms, pour des scripts par exemple
lxc ls volatile.last_state.power=RUNNING --columns n --format csv
```

### Lister les conteneurs à l'arrêt

```bash
# Au format normal
lxc ls volatile.last_state.power=STOPPED
# Et juste les noms
lxc ls volatile.last_state.power=STOPPED --columns n --format csv
```

### Mémoire
```bash
# Déclaration du conteneur à tester
CT=test
# Mémoire actuelle
echo "scale=2; $(lxc query /1.0/containers/$CT/state | jq .memory.usage) / 1024^2" | bc
# Limite de mémoire
lxc query /1.0/containers/smtp | jq '.expanded_config["limits.memory"]'
```

### Test en cours de boucle
```bash
# Mémoire actuelle :
for CT in $(lxc ls volatile.last_state.power=RUNNING --columns n --format csv) ; do
    echo "$CT - $(echo "scale=2; $(lxc query /1.0/containers/$CT/state | jq .memory.usage) / 1024^2" | bc) / $(lxc query /1.0/containers/$CT | jq '.expanded_config["limits.memory"]')"
done
```

#### Récup, à tester et nettoyer
```bash
echo -e "| -------------- | ------------------ | ------------------ |\n|   Conteneur    |     Usage RAM      |    Usage Disque    |\n| -------------- | ------------------ | ------------------ |"; for CT in $(lxc query /1.0/containers | jq '.[]' | sed -e 's/"//g' -e 's,/1.0/containers/,,'); do RAM=$(echo "scale=2; $(lxc query /1.0/containers/$CT/state | jq .memory.usage) / 1024^2" | bc); MAX_RAM=$(lxc query /1.0/containers/$CT | jq '.config["limits.memory"]' | sed 's/"//g'); DISK=$(echo "scale=2; $(lxc query /1.0/containers/$CT/state | jq .disk.root.usage) / 1024^2" | bc); MAX_DISK=$(lxc query /1.0/containers/$CT | jq '.expanded_devices.root.size' | sed 's/"//g'); printf '| %-14.14s | %8.8s Mo / %4.4s | %8.8s Mo / %4.4s |\n' "$CT" "$RAM" "$MAX_RAM" "$DISK" "$MAX_DISK"; done; echo "| -------------- | ------------------ | ------------------ |"
```
