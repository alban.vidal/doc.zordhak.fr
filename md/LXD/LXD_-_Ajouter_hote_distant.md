% LXD - Ajouter un hôte distant

## Mode distant - Configuration

### Activation du mote distant sur l'hôte « source »
```bash
lxc config set core.https_address [::]:8443
lxc config set core.trust_password MonMotDePasse
```

### Ajout de l'hôte « source » depuis l'hôte « distant »
```bash
lxc config set core.https_address [::]:8443

# Utilisation
lxc remote add <ALIAS> <FQDN_ou_IP_distant>

# Exemple
lxc remote add source source.example.com

# Listes les lxd distant
lxc remote list
```

### Désactiver le mots de passe une fois la connexion établie - hôte « source »
```bash
lxc config unset core.trust_password
```

