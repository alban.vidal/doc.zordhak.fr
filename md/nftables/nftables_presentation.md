% Présentation nftables

--------------------------------------------------------------------------------

Fortement inspiré du [Wiki Arch / Nftables](https://wiki.archlinux.org/index.php/Nftables)

## Les bases

### Afficher les règles actuelles

```bash
nft list ruleset
```

### Enregistrer les règles dans le fichier persistant :

```bash
nft list ruleset > /etc/nftables.conf
```

### Recharger les règles

```bash
nft -f /etc/nftables.conf
```

### Activer le service systemd pour charger automatiquement le règles au démarrage

```bash
systemctl enable nftables.service
```

### Si on désire faire de la résolution de nom dans les règles

Il faut charger les règles après le réseau

```bash
# Création du répertoire de surcharge
mkdir /etc/systemd/system/nftables.service.d/

# Surcharge
cat << EOF > /etc/systemd/system/nftables.service.d/override.conf
[Unit]
After=network-online.target
EOF
```

--------------------------------------------------------------------------------

## Configuration complète pas à pas

```bash
# Flush the current ruleset: 
nft flush ruleset

# Add a table: 
nft add table inet filter

# Add the input, forward, and output base chains.
# The policy for input and forward will be to drop.
# The policy for output will be to accept. 
nft add chain inet filter input { type filter hook input priority 0 \; policy drop \; }
nft add chain inet filter forward { type filter hook forward priority 0 \; policy drop \; }
nft add chain inet filter output { type filter hook output priority 0 \; policy accept \; }

# Add two regular chains that will be associated with tcp and udp: 
nft add chain inet filter TCP
nft add chain inet filter UDP

# Related and established traffic will be accepted: 
nft add rule inet filter input ct state related,established accept

# All loopback interface traffic will be accepted: 
nft add rule inet filter input iif lo accept

# Drop any invalid traffic: 
nft add rule inet filter input ct state invalid drop

# Accept ICMP and IGMP: 
# nft add rule inet filter input ip6 nexthdr icmpv6 icmpv6 type '{ destination-unreachable, packet-too-big, time-exceeded, parameter-problem, mld-listener-query, mld-listener-report, mld-listener-reduction, nd-router-solicit, nd-router-advert, nd-neighbor-solicit, nd-neighbor-advert, ind-neighbor-solicit, ind-neighbor-advert, mld2-listener-report }' accept
# nft add rule inet filter input ip protocol icmp icmp type '{ destination-unreachable, router-solicitation, router-advertisement, time-exceeded, parameter-problem }' accept
# nft add rule inet filter input ip protocol igmp accept

# tout ICPM (dont le ping)
#nft add rule inet filter input ip protocol icmp accept

# On autorise le ping
nft add rule inet filter input icmp type echo-request accept

# New udp traffic will jump to the UDP chain: 
nft add rule inet filter input ip protocol udp ct state new jump UDP

# New tcp traffic will jump to the TCP chain: 
nft add rule inet filter input ip protocol tcp tcp flags \& \(fin\|syn\|rst\|ack\) == syn ct state new jump TCP

# Reject all traffic that was not processed by other rules: 
nft add rule inet filter input ip protocol udp reject
nft add rule inet filter input ip protocol tcp reject with tcp reset
nft add rule inet filter input counter reject with icmp type prot-unreachable

# At this point you should decide what ports you want to open to incoming connections,
# which are handled by the TCP and UDP chains.
#
# For example to open connections for a web server add: 
# nft add rule inet filter TCP tcp dport 80 accept
#
# To accept HTTPS connections for a webserver on port 443: 
# nft add rule inet filter TCP tcp dport 443 accept
#
# To accept SSH traffic on port 22: 
# nft add rule inet filter TCP tcp dport 22 accept
#
# To accept incoming DNS requests: 
# nft add rule inet filter TCP tcp dport 53 accept
# nft add rule inet filter UDP udp dport 53 accept
#
# Be sure to make your changes permanent when satisifed. 

################################################################################

# Ouverture du SSH (et log)
nft add rule inet filter TCP tcp dport 22 log accept

# Ouverture NRPE
#nft add rule inet filter TCP tcp dport 5666 ip saddr srv1.example.com accept
#nft add rule inet filter TCP tcp dport 5666 ip saddr srv2.example.com accept
# identique et fusionné :
nft add rule inet filter TCP tcp dport 5666 ip saddr { srv1.example.com, srv2.example.com } accept

# Ouverture SMTP (et log)
# sans compteur :
#nft add rule inet filter TCP tcp dport 25 log accept
# avec compteur :
nft add rule inet filter TCP tcp dport 25 log counter accept
```

--------------------------------------------------------------------------------

## Notes diverses

### Ajouter dans les lors en « warning »

Ajouter « log » dans la règle (avant « accept »)

Exemple :

```bash
nft add rule inet filter TCP tcp dport 22 log accept
```

### Pour les logs, si on veur ajouter un préfixe

Ajouter « log prefix "MON_PREFIXE " » dans la règle (avant « accept »)

```bash
nft add rule inet filter TCP tcp dport 22 log prefix "Nftables_log " accept
```

### Ajouter un compteur

Ajouter « counter » dans la règle (avant « accept »)

Exemple :

```bash
nft add rule inet filter TCP tcp dport 22 counter accept
```
