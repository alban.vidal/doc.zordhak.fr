% Exemple d'un fichier nftables.conf

--------------------------------------------------------------------------------

Extraction complète du fichier `/etc/nftables.conf`

```bash
#!/usr/sbin/nft -f

# file /etc/nftables.conf

################################################################################

flush ruleset

table inet filter {
    chain input {
        type filter hook input priority 0; policy drop;
        ct state established,related accept
        iif "lo" accept
        ct state invalid drop
        icmp type echo-request accept
        ip protocol udp ct state new jump UDP
        ip protocol tcp tcp flags & (fin | syn | rst | ack) == syn ct state new jump TCP
        ip protocol udp reject
        ip protocol tcp reject with tcp reset
        meta nfproto ipv4 counter packets 0 bytes 0 reject with icmp type prot-unreachable
    }

    chain forward {
        type filter hook forward priority 0; policy drop;
    }

    chain output {
        type filter hook output priority 0; policy accept;
    }

    chain TCP {
        # Accepte le SSH
        tcp dport 22 log accept
        # Accepte le NRPE depuis deux serveurs
        tcp dport nrpe ip saddr { srv1.example.com, srv2.example.com } accept
        # Accepte le SMTP
        tcp dport 25 log counter accept
    }

    chain UDP {
    }
}


```
