% Inodes

Voici quelques méthodes pour trouver les répertoires avec le plus d'inodes

----------------------------------------

## Méthode `find`

```bash
find / -xdev -printf '%h\n' | sort | uniq -c | sort -k 1 -n
```

----------------------------------------

## Méthode `du`


Dans les répertoires, lequel est le plus gand

```bash
du --inodes -cs *|sort -h

# Exemple, la HOME avec le plus d'inodes
du --inodes -cs /home/*|sort -h
```

Trouver LE répertoire avec le plus d'inode (va indiquer le sous-répertoire en question)

```bash
du --inodes -cS *|sort -h

# Le répertoire dans toutes les HOMEs avec le plus d'inodex
du --inodes -cS /home/*|sort -h
```

