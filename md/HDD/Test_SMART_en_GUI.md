% Test SMART en GUI

**GSmartControl** - Une alternative à **smartctl** en GUI

Rappel : La commande `smartctl` vient du paquet `smartmontools`.

----------------------------------------

## Paquet à installer

```
sudo apt install gsmartcontrol
```

----------------------------------------

Sous MATE il est disponible via :
+ Applications => Outils système => GSmartControl
