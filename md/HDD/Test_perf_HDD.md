% Tester les perfs d'un disque

----------------------------------------

## hdparm

Test basique :

```bash
sudo hdparm -Tt /dev/sda
```

Avoir plus d'informations :
```bash
sudo hdparm -v /dev/sda
```

----------------------------------------

## fio

Testeur d’E/S flexible

+ Paquet : `fio`

>  Fio est un outil qui générera un certain nombre de threads ou de processus
>  effectuant un type particulier d'action E/S spécifiée par l'utilisateur.
>  Fio prend un certain nombre de paramètres généraux, chacun hérité
>  par les threads à moins d'une configuration particulière de chaque thread
>  qui surcharge les paramètres communs.
>  L'utilisation typique de fio est d'écrire un fichier de travail
>  correspondant à la charge d'E/S que l'on veut simuler.
>  
>  Ce paquet contient l’interface en ligne de commande de fio et tous les outils
>  en ligne de commandes. Le paquet gfio contient l’interface utilisateur GTK+
>  pour fio.

----------------------------------------

## ioping

outil simple de mesure de la latence d’E/S de disque

+  Paquet : `ioping`

>  Ioping surveille la latence des E/S de disque en temps réel. L’idée
>  principale derrière ioping est de procurer un outil semblable à ping
>  affichant la latence d’E/S de la même manière que ping montre la latence du
>  réseau.

----------------------------------------

## Gnome Disks

(application graphique)

+ Paquet : `gnome-disk-utility`
+ Commande : `gnome-disks`
