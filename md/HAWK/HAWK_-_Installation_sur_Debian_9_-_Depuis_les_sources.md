% Hawk - Doc d'installation sur Debian 9 (sources)

[https://github.com/ClusterLabs/hawk](https://github.com/ClusterLabs/hawk)

-----------------------------------------------------------------------------------

### Installation des dépendances
```
apt -y install build-essential git ruby golang libpam0g-dev pkgconf libglib2.0-dev libxml2-dev libcib-dev liblrmd-dev gem bundler libssl1.0-dev ruby-execjs libxslt1-dev corosync-dev libcrmcommon-dev libcrmservice-dev
```

[//]: # "Voir si utile :"
[//]: # "glib-2.0"
[//]: # "glib-networking-tests"
[//]: # "automake"
[//]: # "libcrmcluster-dev"
[//]: # "libxtuplecommon-dev"
[//]: # "gettext"

-----------------------------------------------------------------------------------

### Installation des gem
```
gem install rails
gem install rails -v 4.2.0
gem install puma
gem install puma -v 2.11
gem install sass-rails
gem install hashie -v 3.4.6
gem install virtus
gem install js-routes
gem install tilt -v 1.4.1
gem install fast_gettext
gem install gettext_i18n_rails_js
gem install gettext_i18n_rails
gem install haml-rails
gem install kramdown
gem install web-console
gem install web-console -v 2.3.0
gem install spring
gem install spring -v 1.7.2
gem install uglifier
```

[//]: # "A priori non utile"
[//]: # "gem install bundler"
[//]: # "gem install rake"
[//]: # "gem install rubocop"
[//]: # "gem install brakeman"
[//]: # "gem install rspec-rails"

-----------------------------------------------------------------------------------

### Téléchargement de la dernière version de hawk sur git
```
cd /opt
git clone https://github.com/ClusterLabs/hawk.git
cd /opt/hawk
```

-----------------------------------------------------------------------------------

### Tunning du Makefile
```
sed -i 's/^INIT_STYLE.*/INIT_STYLE = service/' /opt/hawk/Makefile
sed -i 's#\ bin/rake\ # /usr/bin/rake #' /opt/hawk/Makefile
```

-----------------------------------------------------------------------------------

### Compilation !!

Adapter la valeur de l'option -j selon le nombre de processeurs.  
Nombre de processeurs +1  
Ici 2, donc 2+1=3 => -j3

```
make -j3
```

-----------------------------------------------------------------------------------

### Tunning du service systemd
```
#sed -i 's#/etc/sysconfig/#/etc/default/#' /opt/hawk/scripts/hawk-backend.service.in
#sed -i 's#/etc/sysconfig/#/etc/default/#' /opt/hawk/scripts/hawk.service.in
sed -i 's#/etc/sysconfig/#/etc/default/#' /opt/hawk/scripts/hawk.service
sed -i 's#/etc/sysconfig/#/etc/default/#' /opt/hawk/scripts/hawk-backend.service
```

-----------------------------------------------------------------------------------

### Installation
```
make install
```

### Copie de la configuration par défaut dans /etc/default/
```
cp /opt/hawk/rpm/sysconfig.hawk /etc/default/hawk
```

-----------------------------------------------------------------------------------

#### Gestion des droits d'accès au répertoire de travail de Hawk
```
chown -R hacluster:haclient /usr/share/hawk
```

#### Copier les binaires puma et pumactl
```
ln -s /usr/local/bin/puma /usr/bin/puma
ln -s /usr/local/bin/pumactl /usr/bin/pumactl
passwd hacluster
```

-----------------------------------------------------------------------------------

### Installation de hawk-apiserver
#### GIT : https://github.com/krig/hawk-apiserver
```
mkdir /opt/go
export GOPATH=/opt/go
go get -u github.com/krig/hawk-apiserver
```

-----------------------------------------------------------------------------------

### Récupération de compiration de hawk-apiserver
```
cd /opt/go/src/github.com/krig/hawk-apiserver
go build
go test
```

[//]: # "#### C'est moche, mais on en a besoin - sauf si on trouve pourquoi il veut se connecter en SSH à git"
[//]: # "```"
[//]: # "sed -i 's#url = git@github.com:#url = https://github.com/#' /opt/go/src/github.com/krig/hawk-apiserver/.git/config"
[//]: # "sed -i 's#url = git@github.com:#url = https://github.com/#' /opt/go/src/github.com/krig/hawk-apiserver/.gitmodules"
[//]: # "go get -u github.com/krig/hawk-apiserver"
[//]: # "sed -i 's#url = git@github.com:#url = https://github.com/#' /opt/go/src/github.com/krig/hawk-apiserver/.git/modules/vendor/github.com/krig/go-pacemaker/config"
[//]: # "sed -i 's#url = git@github.com:#url = https://github.com/#' /opt/go/src/github.com/krig/hawk-apiserver/vendor/github.com/krig/go-pacemaker/.gitmodules"
[//]: # "go get -u github.com/krig/hawk-apiserver"
[//]: # "```"

#### DEBUG
`grep -R 'git@' /opt/go/src/github.com`

-----------------------------------------------------------------------------------

### Création du lien symbolique pour « installer » hawk-apiserver
```
ln -s /opt/go/bin/hawk-apiserver /usr/sbin/hawk-apiserver
```

-----------------------------------------------------------------------------------

### Génération du certificat
```
cd /etc/hawk
SSLGEN_KEY=hawk.key SSLGEN_CERT=hawk.pem /opt/go/src/github.com/krig/hawk-apiserver/tools/generate-ssl-cert
chown hacluster:haclient /etc/hawk/hawk.pem /etc/hawk/hawk.key
```

-----------------------------------------------------------------------------------

### Gestion des droits
```
chown -R hacluster:haclient /etc/hawk/
chown hacluster:haclient /var/lib/pacemaker

systemctl daemon-reload

systemctl enable hawk.service

systemctl start hawk-backend.service
systemctl start hawk.service
```

-----------------------------------------------------------------------------------

#### AIDE
 + chercher une gem
  `gem list '^hashie$' --remote --all`

-----------------------------------------------------------------------------------

### ACL - voir si utile - à priori non
```
crm configure property enable-acl=true

crm configure role administrator write xpath:"/cib"
crm configure acl_target hacluster administrator

crm configure role admin_config write xpath:"//crm_config"
crm configure acl_target hacluster admin_config
```

