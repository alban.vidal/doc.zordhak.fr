% postqueue

### Afficher la queue actuelle

```bash
# Produce a traditional sendmail-style queue listing.  This option implements the traditional mailq
#   command, by contacting the Postfix showq(8) daemon.
#
postqueue -p
```

### Afficher au format JSON

```bash
# Produce  a  queue  listing  in JSON format, based on output from the showq(8) daemon.
#
postqueue -j
```

### Forcer une nouvelle tentative

```bash
# Forcer tout
# Flush the queue: attempt to deliver all queued mail.
#
postqueue -f

# Avec l'ID
# Schedule immediate delivery of deferred mail with the specified queue ID.
postqueue -i <ID>
```

### Vider (effacer) la queue

```bash
postsuper -d ALL
```
