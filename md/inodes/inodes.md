% Les inodes sous GNU/Linux

## Afficher les inode disponibles par partition

```
df -i
```

## Afficher les répertoires avec le plus d'inodes

À partir de la racine, affiche le répertoire contenant le plus d'inodes en bas

```
find / -xdev -printf '%h\n' | sort | uniq -c | sort -k 1 -n
```
