% Installation de Minikube

Testé sur :

+ [x] Debian 9

--------------------

+ Installer KVM et ajouter l'utilisateur dans le groupe libvirt
    ```bash
    sudo apt-get update
    sudo apt-get install libvirt-clients libvirt-daemon-system qemu-kvm
    sudo usermod -a -G libvirt $(whoami)
    ```

+ Mettre à jour la session actuelle pour prendre en compte le nouveau groupe
    ```bash
    sudo newgrp libvirt
    ```

+ Télécharger le driver KVM 2 pour Minikube
    ```bash
    curl -LO https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-kvm2
    chmod +x docker-machine-driver-kvm2
    sudo mv docker-machine-driver-kvm2 /usr/local/bin/
    ```

+ Télécharger Minikube
    ```bash
    curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
    chmod +x minikube
    sudo mv minikube /usr/local/bin/
    ```

+ Vérifier l'installation
    ```bash
    minikube version
    ```

+ Installer Kubectl - Méthode 1 : Dépôt + Paquet
    ```bash
    apt-get update && apt-get install -y apt-transport-https
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
    cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
    deb http://apt.kubernetes.io/ kubernetes-xenial main
    EOF
    apt-get update
    apt-get install -y kubectl
    ```

+ Installer Kubectl - Méthode 2 : Télécharger le binaire
    ```bash
    # Télécharger la dernière version
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/darwin/amd64/kubectl
    # Définir le binaire comme exécutable
    chmod +x ./kubectl
    # Déplacement du binaire
    sudo mv ./kubectl /usr/local/bin/kubectl
    ```

+ Démarrer Minikube
    ```bash
    minikube start --vm-driver kvm2
    ```

+ Vérifier la version de Kubernetes
    ```bash
    kubectl version
    ```

+ Info du cluster Kubernetes
    ```bash
    kubectl cluster-info
    ```

+ Lister les noeuds
    ```bash
    kubectl get nodes
    ```

+ Afficher de Dashbord
    ```
    minikube dashboard
    ```

# Liens

+ [Github Minikube](https://github.com/kubernetes/minikube)
+ [Github Minikube - Driver Minikube KVM2](https://github.com/kubernetes/minikube/blob/master/docs/drivers.md#kvm2-driver)
+ [Github Minikube - Docs](https://github.com/kubernetes/minikube/tree/master/docs)
+ [Installation Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
