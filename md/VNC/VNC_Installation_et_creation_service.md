% VNC - Installation et configuration

# Installation du paquet

```
apt install x11vnc
```

# Génération du mot de passe

```
x11vnc -storepasswd yourVNCpasswordHERE /etc/x11vnc.pass
```

# Création de l'unit-file systemd

Fichier : `/etc/systemd/system/x11vnc.service`

Contenu :
```
[Unit]
Description="x11vnc"
Requires=display-manager.service
After=display-manager.service

[Service]
ExecStart=/usr/bin/x11vnc -display :0 -forever -shared -rfbauth /etc/x11vnc.pass -auth guess -rfbport 5900
ExecStop=/usr/bin/killall x11vnc
Restart=on-failure
Restart-sec=2

[Install]
WantedBy=multi-user.target
```

# Activation du service

```
systemctl daemon-reload
systemctl enable --now x11vnc
```

# Exemple de client

+ Remmina  - `apt install remmina-plugin-rdp`
