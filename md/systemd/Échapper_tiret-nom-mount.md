% Échapper un tiret dans un .mount

Pour monter la pratition /srv/toto/tata-titi, le nom du service .mount doit être échappé pour prendre en compte les tirets.


```bash
systemd-escape -p /srv/toto/tata-titi
# srv-toto-tata\x2dtiti
```

Le nom du service sera donc srv-toto-tata\x2dtiti.mount

### Exemple de service .mount

```bash
# cat "/etc/systemd/system/srv-toto-tata\x2dtiti.mount"
[Unit]
Description=Mount lvm /srv/toto/tata-titi
# Le démarrer après un autre service, ici lxd
Before=snap.lxd.daemon.service
# Permet d'arrêter le service avant un shutdown ou un reboot
Conflicts=shutdown.target reboot.target halt.target

[Mount]
What=/dev/vg_data/lv_srv_toto_tata-titi
Where=/srv/toto/tata-titi
Type=ext4
# - Options comprises dans « defaults »
#   man mount
#   Use the default options: rw, suid, dev, exec, auto, nouser, and async.
# 
# - Conseillé pour un ssd : noatime
#   Ne pas mettre à jour les horodatages d'accès aux inœuds sur ce système de fichiers
#   (accès plus rapide pour des serveurs de news par exemple).
Options=defaults

[Install]
WantedBy=multi-user.target
```

### Activer au démarrage et démarrer le service .mount

```bash
systemctl enable --now "srv-toto-tata\x2dtiti.mount"
# ou
systemctl enable --now srv-toto-tata\\x2dtiti.mount
```
