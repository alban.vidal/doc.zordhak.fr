% Meetup Debian

**Reprenez le contrôle de vos données avec votre cloud auto-hébergé**

![Logo Debian](https://dl.zordhak.fr/Meetup/logo-debian.jpg)

--------------------

## Sommaire

+ Présentation Nextcloud
<!-- .element: class="fragment" -->
+ Présentation LXC / LXD
<!-- .element: class="fragment" -->
+ Démo d'installation automatisée
<!-- .element: class="fragment" -->
+ Démo d'utilisation Nextcloud
<!-- .element: class="fragment" -->
+ Démo d'utilisation LXD
<!-- .element: class="fragment" -->
+ Liens
<!-- .element: class="fragment" -->

----------------------------------------

# Présentation Nextcloud

--------------------

## Présentation Nextcloud

+ Programme Open-Source type client - serveur
<!-- .element: class="fragment" -->
+ Fonctionnalités de base similaires à Dropbox
<!-- .element: class="fragment" -->
+ Auto-hébergement, permettant d'avoir le contrôle sur ses données
<!-- .element: class="fragment" -->
+ Partage entre utilisateurs, en public ou entre cloud
<!-- .element: class="fragment" -->
+ Greffons (agenda, contact, édition en ligne...)
<!-- .element: class="fragment" -->

![Logo Nextcloud](https://dl.zordhak.fr/Meetup/logo-nextcloud.jpg)

----------------------------------------

# Présentation LXC / LXD

--------------------

## Virtualisation / Para-Virtualisation / Isolation

![Virtualisation](https://upload.wikimedia.org/wikipedia/commons/5/5c/Diagramme_ArchiEmulateur.png?uselang=fr)<!-- .element height="30%" width="30%" -->
![Para-Virtualisation](https://upload.wikimedia.org/wikipedia/commons/f/fa/Diagramme_ArchiHyperviseur.png?uselang=fr)<!-- .element height="30%" width="30%" -->
![Isolation](https://upload.wikimedia.org/wikipedia/commons/3/38/Diagramme_ArchiIsolateur.png?uselang=fr =100)<!-- .element height="30%" width="30%" -->

+ Hyperviseur de type 2 : VirtualBox, VMware Fusion,…
<!-- .element: class="fragment" -->
+ Hyperviseur de type 1 : KVM, VMware ESX,…
<!-- .element: class="fragment" -->
+ Isolateurs : LXC, Docker,…
<!-- .element: class="fragment" -->

Source : [Wikipedia](https://fr.wikipedia.org/wiki/Virtualisation)

--------------------

## Présentation LXC

+ LXC permet d'isoler plusieurs SE sur même hôte
<!-- .element: class="fragment" -->
+ Similaire Docker, mais pour un environnement complet
<!-- .element: class="fragment" -->
+ Virtualisation de l'environnement d'exécution
<!-- .element: class="fragment" -->
  - réseau, mémoire, processus, système de fichier...
+ Utilise le même noyau que l'hôte
<!-- .element: class="fragment" -->
+ Ressemble à une machine virtuelle
<!-- .element: class="fragment" -->

![Logo LXD](https://dl.zordhak.fr/Meetup/logo-lxd.png)

--------------------

## Présentation LXD

+ LXD démon pour manipuler plus facilement les conteneurs LXC
<!-- .element: class="fragment" -->
+ Pas encore présent dans les dépôts Debian (voir wiki page LXD)
<!-- .element: class="fragment" -->
+ Installation facile via snapd sur la plupart des distributions
<!-- .element: class="fragment" -->
+ Nombreuses images SE disponibles :
<!-- .element: class="fragment" -->
  - Debian, Ubuntu, Centos, Fedora, Alpine, Oracle, Gentoo, Opensuse, ...

----------------------------------------

# Démo d'installation

--------------------

## Prérequis

+ Enregistrements DNS type A et PTR (Nom <-> IP) cloud
```bash
host cloud.gg42.eu
host 51.15.240.32
```
+ Enregistrement DNS type A (Nom -> IP) collabora
```bash
host collabora.gg42.eu
```

--------------------

## Démo d'installation cloud

+ Démo d'une installation complète automatisée via scripts shell
+ [Lien vers le dépôt Github](https://github.com/AlbanVidal/cloud_full)

```bash
# Installation de git
apt update && apt install -y git
# Clonage du dépôt
git clone https://github.com/AlbanVidal/cloud_full.git
cd cloud_full
# Configuration de l'hôte, installation de lxd via snapd
./10_install_start.sh
# Création et configuration des conteneurs
./11_install_next.sh
```

--------------------

## Présentation architecture

+ Différents composants isolés les uns des autres via des conteneurs lxd
<!-- .element: class="fragment" -->

![Architecture](https://dl.zordhak.fr/Meetup/cloud.png)
<!-- .element: class="fragment" -->

----------------------------------------

# Démo Nextcloud

--------------------

### Nextcloud Web
+ Gestion utilisateurs
+ Gestion fichiers
+ Agenda et Contacts
+ Partage entre utilisateurs, par lien public et entre cloud
+ Édition de documents LibreOffice en ligne
+ Appels

--------------------

### Nextcloud client Bureau
+ Synchro de documents via l'[application de bureau](https://nextcloud.com/install/#install-clients)
+ Synchro agenda dans Thunderbird
+ Synchro contacts dans Thunderbird avec le [Greffon CardBook](https://addons.mozilla.org/fr/thunderbird/addon/cardbook/)

--------------------

### Nextcloud client Android
+ Installation et utilisation rapide
+ Synchro des photos dans son cloud
+ Synchro contacts et agenda via [DAVdroid](https://www.davdroid.com)

--------------------

### Mon utilisation
+ Agenda
+ Contacts
+ Gestion centralisée des mots de passe ([Buttercup](http://buttercup.pw/))
+ Synchro photos téléphone
+ Archivage de documents
+ Partage de documents / photos
+ Hébergé chez [Scaleway](https://www.scaleway.com/)
+ Synchronisation journalière [Rsync versionné maison](https://github.com/AlbanVidal/backup)

--------------------

### Scan de sécurité
+ Test SSL avec [SSL Labs](https://www.ssllabs.com/ssltest)
+ Test Nextcloud avec [Scan Nextcloud](http://scan.nextcloud.com)

----------------------------------------

# Démo LXD

--------------------

## Démo LXD

+ LXD est déjà installé sur deux hôtes
<!-- .element: class="fragment" -->
  - demo
<!-- .element: class="fragment" -->
  - cloud
<!-- .element: class="fragment" -->

--------------------

### LXD

#### Création de nouveaux conteneurs

```bash
# Depuis l'hôte cloud

# Lister les conteneurs
lxc ls

# Créer et démarrer des conteneurs
lxc launch images:debian/stretch debian-9
lxc launch images:debian/buster debian-10

# Créer sans démarrer
lxc init images:ubuntu/18.04 ubuntu-1804
lxc init images:centos/7 centos-7
```

--------------------

### LXD

#### Commandes de base
```bash
# Copier un conteneur
lxc copy debian-10 d10

# Démarrer le conteneur copié
lxc start d10

# Entrer dans un shell
lxc shell d10

# Exécuter une commande simple
lxc exec d10 -- hostname

# Exécuter une commande plus complexe
lxc exec d10 -- bash -c "hostname;date && uptime"
```

--------------------

### LXD

#### Copier un fichier depuis - vers un conteneur
```bash
# Copier un fichier vers le conteneur
lxc file push /etc/hosts d10/tmp/test-push
lxc exec d10 -- cat /tmp/test-push

# Récupération d'un fichier du conteneur
lxc file pull d10/etc/network/interfaces d10-interfaces
cat d10-interfaces
```

--------------------

### LXD

#### Snapshot

```bash
# Création d'un snapshot
lxc snapshot d10 initial
lxc ls d10

# Destruction volontaire de fichiers
# Couche 8 du modèle OSI
lxc exec d10 -- rm -rf /etc

# Vérification de la grosse boulette
lxc exec d10 -- ls -l /etc

# Restauration du snapshot
lxc restore d10 initial

# Vérification de la restauration
lxc exec d10 -- ls -l /etc

# Supprimer le snapshot
lxc delete d10/initial
```

--------------------

### LXD

#### Limitation des ressources

```bash
# Limiter la mémoire
lxc exec d10 -- free -m
lxc config set d10 limits.memory 128MB
lxc exec d10 -- free -m

# Limiter le nombre de cpu
lxc shell d10
## top
lxc config set d10 limits.cpu 1
lxc shell d10

# Supprimer les limites
lxc config unset d10 limits.memory
lxc config unset d10 limits.cpu
```

--------------------

### LXD

#### Profils

```bash
# Lister les conteneurs avec les profils attachés
lxc ls --fast

# Lister les profils
lxc profile list

# Ajouter un profil
lxc profile create 1cpu512ram
lxc profile set 1cpu512ram limits.cpu 1
lxc profile set 1cpu512ram limits.memory 512MB

# Afficher le profil
lxc profile show 1cpu512ram

# Ajouter le profil à un conteneur
lxc profile add d10 1cpu512ram

# Supprimer les profil
lxc profile delete 1cpu512ram
```

--------------------

### LXD

#### Info et config d'un conteneur

```bash
# Status, type, profils, processus, mémoiré, réseau, snapshots...
lxc info d10

# Image, type (éphémère ou non), profils...
lxc config show d10
```

--------------------

### LXD

#### Mode distant - Configuration

```bash
# Activation du mote distant sur l'hôte « demo »
lxc config set core.https_address [::]:8443
lxc config set core.trust_password MonMotDePasse

# Ajout de l'hôte « demo » depuis l'hôte « cloud »
lxc config set core.https_address [::]:8443
lxc remote add demo demo.gg42.eu
lxc remote list
```

--------------------

### LXD

#### Mode distant - Utilisation

```bash
# Lister les conteneurs sur l'hôte distant
lxc list demo:

# Actions sur un conteneur distant
lxc exec demo:rvprx -- hostname

# Ouvrir un shell sur un conteneur distant
lxc shell demo:rvprx

# Déplacer un conteneur
lxc stop d10
lxc profile remove d10 1cpu512ram
lxc move local:d10 demo:
lxc list
lxc list demo:

# Ouvrir un shel sur le conteneur copié
lxc start demo:d10
lxc shell demo:d10
```

--------------------

### LXD

#### Installation avec snap

```bash
# Installation de snapd
apt update && apt install snapd udev btrfs-tools

# Installation lxd via snap en version LTS
## version 2.0 (11/04/2016) Juin 2021
## version 3.0 (02/04/2018) Juin 2023
snap install lxd --channel=3.0

# Initialisation lxd
lxd init
```

--------------------

### LXD

#### Installation avec .deb maison

NE PAS UTILISER EN PRODUCTION - TESTS SEULEMENT

+ lxd1
+ lxd2

```bash
# Ajout clé dépôt
wget -O - http://deb.zordhak.fr/gpg.key|apt-key add -

# Ajout dépôt
wget -O /etc/apt/sources.list.d/deb.zordhak.fr.list http://deb.zordhak.fr/deb.zordhak.fr.list

# Installation LXD
apt update && apt install lxd udev btrfs-tools

# Initialisation lxd
lxd init

# Reboot pour prise en compte utilisateur non privilégié
systemctl reboot
```

--------------------

### LXD et criu

#### checkpoint and restore in userspace

+ Outils permettant de figer une application, sauvegarder puis restaurer
<!-- .element: class="fragment" -->
+ Permet de migrer un conteneur à chaud
<!-- .element: class="fragment" -->
+ Disponible sur les dépôts Unstable « sid »
<!-- .element: class="fragment" -->
+ À faire sur les deux hôtes
<!-- .element: class="fragment" -->

```bash
# Ajout du dépôt Sid
echo 'deb http://ftp.fr.debian.org/debian/ sid main' > /etc/apt/sources.list.d/sid.list

# Changement de la priorité du dépôt sid
echo -e 'Package: *\nPin: release a=unstable\nPin-Priority: 50' > /etc/apt/preferences.d/sid

# Installation de criu
apt update && apt install criu
```
<!-- .element: class="fragment" -->

--------------------

### LXD et criu

##### Migration d'un conteneur à chaud entre deux isolateur

###### Utilisation
```bash
lxc move [<remote>:]<container>[/<snapshot>] [<remote>:][<container>[/<snapshot>]] [--container-only]
```
--------------------

### LXD et criu

##### Migration d'un conteneur à chaud entre deux isolateur

###### Préparation
```bash
# Création d'un conteneur sur « lxd1 » et installation openssh
lxc launch images:debian/buster d10
lxc exec d10 -- bash -c "apt update && apt install openssh-server screen"

# Mot de passe root et configuration ssh POUR LES TESTS !
lxc exec d10 -- bash -c "passwd"
lxc exec d10 -- sed -i 's/#\(PermitRootLogin\).*/\1 yes/' /etc/ssh/sshd_config
lxc exec d10 -- systemctl restart sshd

# Connexion ssh et ouverture de screen
ssh root@<IP-d10>
  # Dans la session ssh
  screen
    # Dans le screen
    top

# Détacher le screen, sortir du conteneur
<Ctrl> + a d
exit
```

--------------------

### LXD et criu

##### Migration d'un conteneur à chaud entre deux isolateur

###### Migration

```bash
# Ajouter « lxd2 » depuis « lxd1 »
lxc remote add lxd2 lxd2.gg42.eu

# Déplacement du conteneur
lxc move d10 lxd2:

# Depuis le noeud nextcloud se connecter en ssh et ouvrir screen
ssh root@<IP-d10>
  # Dans la session ssh
  screen -ls
  screen -x
```

----------------------------------------

# Liens

--------------------

### Nextcloud
+ [Site officiel](https://nextcloud.com)
+ [Github](https://github.com/nextcloud)
+ [Application Android](https://play.google.com/store/apps/details?id=com.nextcloud.client)
+ [Application Android Nextcloud Talk](https://play.google.com/store/apps/details?id=com.nextcloud.talk2)
+ [Application Linux/Max/Windows Nextcloud](https://nextcloud.com/install/#install-clients)

--------------------

### Collabora Online
+ [Site officiel - Collabora Online](https://www.collaboraoffice.com/collabora-online)
+ [Nextcloud - Collabora](https://nextcloud.com/collaboraonline)
+ [Github Greffon Collabora pour Nextcloud](https://github.com/nextcloud/richdocuments)
+ [Github Libreoffice Online (ro)](https://github.com/LibreOffice/online)

--------------------

### LXC / LXD
+ [Site officiel - LXD](https://linuxcontainers.org/lxd)
+ [Github LXD](https://github.com/lxc/lxd)
+ [The LXD container hypervisor (Ubuntu)](https://www.ubuntu.com/containers/lxd)
+ [Blog Stéphane Graber](https://stgraber.org/2016/03/11/lxd-2-0-blog-post-series-012/)

--------------------

### Autres liens

+ [Wiki Debian LXD](https://wiki.debian.org/LXD)
+ [Mozilla SSL Configuration Generator](https://mozilla.github.io/server-side-tls/ssl-config-generator/)
+ [Site DAVdroid](https://www.davdroid.com)
+ [Application Android DAVdroid](https://play.google.com/store/apps/details?id=at.bitfire.davdroid)
+ [Greffon CardBook Thunderbird](https://addons.mozilla.org/fr/thunderbird/addon/cardbook/)
+ [Sauvegarde cloud rsync](https://github.com/AlbanVidal/backup)
+ [Présentation HTML Javascript - Reveal.JS](https://revealjs.com)

----------------------------------------

# Questions ?

--------------------

## Télécharger au format présentation

```bash
git clone https://github.com/hakimel/reveal.js.git
cd reveal.js
wget -q https://doc.zordhak.fr/meetup-md -O Présentation_Meetup_cloud_privé.md
wget -q https://doc.zordhak.fr/meetup-html -O Présentation_Meetup_cloud_privé.html
```

--------------------

# FIN

[doc.zordhak.fr/meetup](https://doc.zordhak.fr/meetup)

Alban Vidal
