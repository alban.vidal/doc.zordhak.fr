% Stéganographie sur une image

----------------------------------------

## En CLI

### steghide

Site [http://steghide.sourceforge.net/](http://steghide.sourceforge.net/)

Doc [http://steghide.sourceforge.net/documentation/manpage.php](http://steghide.sourceforge.net/documentation/manpage.php)

```
cat << EOF > embed.txt
Ici c'est mon texte
bla bla bla
EOF
```

#### Embed secret data in a cover file thereby creating a stego file (ajouter une phrase)
```
steghide embed -cf image.jpg -ef embed.txt
```

#### Extract secret data from a stego file (récupérer une phrase)
```
steghide --extract -sf image.jpg
```

#### Display information about a cover or stego file
```
steghide info image.jpg
```

----------------------------------------

## En graphique

### stegosuite

Dispo sur Debian
```
sudo apt install stegosuite
```
