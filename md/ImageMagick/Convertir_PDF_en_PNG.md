% Convertir un PDF en PNG

Utilitaire à utiliser : **imagemagick**

--------------------

## Installation

```
sudo apt install imagemagick
```

--------------------

## Configuration

> Note pour Debian 10 (testé sous Debian 11 et pas besoin de modifier la conf)

Pour convertir les PDF en PNG, il faut modifier la conf ImageMagick, exemple sous Debian 10 (Buster) :

```
sudo sed -i '/^<\/policymap>$/ i \ \ <policy domain="coder" rights="read | write" pattern="PDF" />' /etc/ImageMagick-6/policy.xml
```

Pour explication de la commande, il faut ajouter la ligne `<policy domain="coder" rights="read | write" pattern="PDF" />` avant la ligne `</policymap>`.

--------------------

## Conversion PDF en PNG

```
convert -density 200 fichier-in.pdf -quality 90 -flatten fichier-out.png
```

Options :
+ `-flatten` pour avoir le transparent en blanc
